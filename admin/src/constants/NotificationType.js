/**
 * Available Notification types
 */

export const NotificationType = {
  LIKE: 'LIKE',
  FOLLOW: 'FOLLOW',
  HIRE: 'HIRE',
  COMMENT: 'COMMENT',
};
