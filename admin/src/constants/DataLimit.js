/**
 * Data limit constants for infinite scroll
 */

export const HOME_PAGE_POSTS_LIMIT = 6;

export const EXPLORE_PAGE_POSTS_LIMIT = 15;

export const PEOPLE_PAGE_USERS_LIMIT = 15;

export const STORE_PAGE_HIRERS_LIMIT = 20;

export const NOTIFICATIONS_PAGE_NOTIFICATION_LIMIT = 50;

export const PROFILE_PAGE_POSTS_LIMIT = 6;
