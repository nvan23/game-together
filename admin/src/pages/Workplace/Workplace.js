import React, { useEffect, Fragment } from 'react';
import { Link, generatePath } from 'react-router-dom';
import styled from 'styled-components';
import { useApolloClient } from '@apollo/client';

import { Container } from 'components/Layout';
import Skeleton from 'components/Skeleton';
import Empty from 'components/Empty';
import Head from 'components/Head';

import { timeAgo } from 'utils/date';

import {
  Table,
  Typography,
  Avatar,
  Image,
  Steps,
  Divider,
  Button,
  notification,
  Tag,
} from 'antd';

import {
  DownOutlined,
  UpOutlined,
  CheckCircleOutlined,
  SyncOutlined,
  CloseCircleOutlined,
  ClockCircleOutlined,
  CheckOutlined,
} from "@ant-design/icons";

import CountDown from 'ant-design-pro/lib/CountDown';

import {
  GET_ALL_REPORTS,
  GET_ALL_HIRES_TO_WORK,
  GET_ALL_HIRES_TO_WORK_SUBSCRIPTION,
  CHANGE_READY_CLIENT_STATUS,
  GET_HIRE,
  REFUND_REQUEST_TO_LESSOR,
} from 'graphql/hire';

import { PEOPLE_PAGE_USERS_LIMIT } from 'constants/DataLimit';
import { WAIT, ONGOING, CLOSE } from 'constants/Hire';

import { useQuery } from '@apollo/client';

import { useStore } from 'store';

import * as Routes from 'routes';

import { A } from 'components/Text';

const Root = styled(Container)`
  margin-top: ${(p) => p.theme.spacing.lg};

  @media (min-width: ${(p) => p.theme.screen.lg}) {
    margin-left: ${(p) => p.theme.spacing.lg};
    padding: 0;
  }
`;

const PeopleContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(180px, 3fr));
  grid-auto-rows: auto;
  grid-gap: 20px;
  margin-bottom: ${(p) => p.theme.spacing.lg};
`;

const Message = styled(Link)`
  text-decoration: none;
  font-size: ${(p) => p.theme.font.size.xs};
`;

const Name = styled.span`
  font-size: ${(p) => p.theme.font.size.xs};
  font-weight: ${(p) => p.theme.font.weight.bold};
  color: ${(p) => p.theme.colors.primary.main};
`;

const ExpandableContainer = styled.div`
  padding: 20px 40px;
`
const StepContainer = styled.div`
  padding: 0 0 10px 30px;
`
const ButtonStepContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin: 20px 0 0 30px;
`

const ButtonContainer = styled.div`
  margin-left: 20px;
`

/**
 * Workplace where player receive hires 
 */
const Workplace = () => {
  const [{ auth }] = useStore();
  const client = useApolloClient();

  const { subscribeToMore, data, loading, networkStatus } = useQuery(GET_ALL_REPORTS, {
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
  });

  console.log(data)

  useEffect(() => {
    const unsubscribe = subscribeToMore({
      document: GET_ALL_HIRES_TO_WORK_SUBSCRIPTION,
      variables: { authUserId: auth.user.id },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const newHireToWork = subscriptionData.data.hireCreated;
        const mergedHiresToWork = [...prev.getAllHiresToWork, newHireToWork];

        return { getAllHiresToWork: mergedHiresToWork };
      },
    });

    return () => {
      unsubscribe();
    };
  }, [auth.user.id, subscribeToMore]);

  console.log(data)


  const { Text, Title } = Typography;
  const { Step } = Steps;

  const renderContent = () => {
    if (loading && networkStatus === 1) {
      return (
        <PeopleContainer>
          <Skeleton height={280} count={PEOPLE_PAGE_USERS_LIMIT} />
        </PeopleContainer>
      );
    }

    const reports = data.getAllReports;

    let countIndex = 1;

    const dataSourceReportTable = reports.map((report) => {
      const unitReport = {
        index: countIndex++,
        key: report.id,
        hireId: report.hireId.id,
        price: report.hireId.price,
        hours: report.hireId.hours,
        cost: report.hireId.cost,
        status: report.hireId.status,
        userId: report.clientId.id,
        username: report.clientId.username,
        avatar: report.clientId.image,
        responded: report.responded,
        content: report.content,
        refundRequest: report.refundRequest,
        createdReportAt: report.createdAt,
        createdHireAt: report.hireId.createdAt,
      }
      return unitReport;
    })

    let uniqueUsernameClient = [...new Set(reports.map((report) => report.clientId.username))];
    const usernameClientToFilter = uniqueUsernameClient.map((username) => {
      const user = {
        text: username,
        value: username,
      }
      return user;
    });

    const RefundRequestToFilter = [
      {
        text: 'Yes',
        value: true,
      },
      {
        text: 'No',
        value: false,
      },
    ];

    const statusToFilter = [
      {
        text: 'Ongoing',
        value: 'ongoing',
      },
      {
        text: <Text type="success">Success</Text>,
        value: 'success',
      },
      {
        text: <Text type="danger">Fail</Text>,
        value: 'fail',
      },
    ];

    const columns = [
      {
        title: 'Index',
        dataIndex: 'index',
        key: 'index',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.index - b.index,
      },
      {
        title: 'Avatar',
        dataIndex: 'avatar',
        key: 'avatar',
        align: 'center',
        render: avatar => (
          <Avatar
            src={<Image src={avatar} />}
            size={40}
          />
        )
      },
      {
        title: 'User',
        dataIndex: 'username',
        key: 'username',
        align: 'center',
        filters: usernameClientToFilter,
        onFilter: (value, record) => record.username.indexOf(value) === 0,
        render: username => (
          <A to={generatePath(Routes.USER_PROFILE, { username: username })}>
            <Name>{username}</Name>
          </A>
        )
      },
      {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.price - b.price,
        render: price => (`$${price}`)
      },
      {
        title: 'Hours',
        dataIndex: 'hours',
        key: 'hours',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.hours - b.hours,
      },
      {
        title: 'Cost',
        dataIndex: 'cost',
        key: 'cost',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.cost - b.cost,
        render: cost => (`$${cost}`)
      },
      {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
        align: 'center',
        filters: statusToFilter,
        onFilter: (value, record) => record.status.indexOf(value) === 0,
        render: status => (
          status === 'success'
            ? (<Tag icon={<CheckCircleOutlined />} color="success">Success</Tag>)
            : (status === 'fail'
              ? (<Tag icon={<CloseCircleOutlined />} color="error">Fail</Tag>)
              : (<Tag icon={<SyncOutlined />} color="processing">Ongoing</Tag>))
        )
      },
      {
        title: 'Refund Request',
        dataIndex: 'refundRequest',
        key: 'refundRequest',
        align: 'center',
        width: 110,
        filters: RefundRequestToFilter,
        onFilter: (value, record) => record.isHirer === value,
        render: refundRequest => (
          refundRequest
            ? <CheckOutlined />
            : ''
        ),
      },
      {
        title: 'Created Hire At',
        dataIndex: 'createdHireAt',
        key: 'createdHireAt',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => parseInt(a.createdHireAt) - parseInt(b.createdHireAt),
        render: createdHireAt => (
          <>
            {/* for Vietnam, set value in toLocalDateString is "vi-VN": "dd/MM/yyyy" */}
            {`${timeAgo(createdHireAt)} ago (${new Date(parseInt(createdHireAt)).toLocaleDateString("en-US")})`}
          </>
        )
      },
      ,
      {
        title: 'Created Report At',
        dataIndex: 'createdReportAt',
        key: 'createdReportAt',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => parseInt(a.createdReportAt) - parseInt(b.createdReportAt),
        render: createdReportAt => (
          <>
            {/* for Vietnam, set value in toLocalDateString is "vi-VN": "dd/MM/yyyy" */}
            {`${timeAgo(createdReportAt)} ago (${new Date(parseInt(createdReportAt)).toLocaleDateString("en-US")})`}
          </>
        )
      },
      {
        title: 'Action',
        dataIndex: 'key',
        key: 'key',
        align: 'center',
        fixed: 'right',
        width: 100,
        render: (record) => (
          <>
            <Button
              type="link"
              onClick={
                async () => {
                  try {
                    notification.info({
                      message: `Sending request refund successfully`,
                    });
                    await client.mutate({
                      mutation: REFUND_REQUEST_TO_LESSOR,
                      variables: {
                        hireId: record.hireId,
                      }
                    });
                  } catch (err) { }
                }
              }
            >
              Send to lessor
            </Button>
            <Button type="link">Done</Button>
          </>
        )
      },
    ];

    let totalHours = 0;
    let totalCost = 0;

    dataSourceReportTable.forEach(({ hours, cost }) => {
      totalHours += hours;
      totalCost += cost;
    });

    return (
      <Fragment>
        <Table
          bordered
          size="middle"
          rowKey="key"
          columns={columns}
          expandable={{
            expandedRowRender: record => (
              <ExpandableContainer>
                <Title level={5}>{`REPORT ID: ${record.key.toUpperCase()}`}</Title>
                <Divider />
                <Title level={5}>{`Content: ${record.content}`}</Title>
              </ExpandableContainer>
            ),
            expandIcon: ({ expanded, onExpand, record }) =>
              expanded
                ? (<UpOutlined onClick={e => onExpand(record, e)} />)
                : (<DownOutlined onClick={e => onExpand(record, e)} />),
          }}
          dataSource={dataSourceReportTable}
          summary={
            pageData => {
              let totalHoursOnPage = 0;
              let totalCostOnPage = 0;
              let totalCountIndexOnPage = 0;

              pageData.forEach(({ hours, cost }) => {
                totalCountIndexOnPage++;
                totalHoursOnPage += hours;
                totalCostOnPage += cost
              });
              return (
                <>
                  { totalCountIndexOnPage > 10 &&
                    <Table.Summary.Row>
                      <Table.Summary.Cell />
                      <Table.Summary.Cell align='center'>
                        <Text type="danger">{totalCountIndexOnPage}</Text>
                      </Table.Summary.Cell>
                      <Table.Summary.Cell colSpan={3} />
                      <Table.Summary.Cell align='center'>
                        <Text type="danger">{totalHoursOnPage}</Text>
                      </Table.Summary.Cell>
                      <Table.Summary.Cell align='center'>
                        <Text type="danger">${totalCostOnPage}</Text>
                      </Table.Summary.Cell>
                      <Table.Summary.Cell colSpan={2} />
                      <Table.Summary.Cell align='center'>
                        Total / Page
                    </Table.Summary.Cell>
                    </Table.Summary.Row>
                  }
                  <Table.Summary.Row>
                    <Table.Summary.Cell />
                    <Table.Summary.Cell align='center'>
                      <Text type="danger">{countIndex - 1}</Text>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={3} />
                    <Table.Summary.Cell align='center'>
                      <Text type="danger">{totalHours}</Text>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell align='center'>
                      <Text type="danger">${totalCost}</Text>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={4} />
                    <Table.Summary.Cell align='center'>
                      Total
                    </Table.Summary.Cell>
                  </Table.Summary.Row>
                </>
              );
            }}
        />
      </Fragment>
    )
  };

  return (
    <Root maxWidth="md">
      <Head title="Reports" />

      {renderContent()}
    </Root>
  );
};

export default Workplace;
