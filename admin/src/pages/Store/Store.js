import React, { Fragment } from 'react';
import styled from 'styled-components';

import { Container } from 'components/Layout';
import Skeleton from 'components/Skeleton';
import { Loading } from 'components/Loading';
import Empty from 'components/Empty';
import InfiniteScroll from 'components/InfiniteScroll';
import Head from 'components/Head';
import PeopleCard from './StoreCard';

import { GET_ALL_HIRERS } from 'graphql/user';

import { STORE_PAGE_HIRERS_LIMIT } from 'constants/DataLimit';

import { useQuery } from '@apollo/client';

const Root = styled(Container)`
  margin-top: ${(p) => p.theme.spacing.lg};

  @media (min-width: ${(p) => p.theme.screen.lg}) {
    margin-left: ${(p) => p.theme.spacing.lg};
    padding: 0;
  }
`;

const StoreContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(180px, 3fr));
  grid-auto-rows: auto;
  grid-gap: 20px;
  margin-bottom: ${(p) => p.theme.spacing.lg};
`;

/**
 * Store page
 */
const Store = () => {
  const variables = {
    skip: 0,
    limit: STORE_PAGE_HIRERS_LIMIT,
  };
  const { data, loading, fetchMore, networkStatus } = useQuery(GET_ALL_HIRERS, {
    variables,
    notifyOnNetworkStatusChange: true,
  });

  const renderContent = () => {
    if (loading && networkStatus === 1) {
      return (
        <StoreContainer>
          <Skeleton height={280} count={STORE_PAGE_HIRERS_LIMIT} />
        </StoreContainer>
      );
    }

    const { users, count } = data.getAllHirers;
    if (!users.length > 0) return <Empty text="No people yet." />;

    return (
      <InfiniteScroll
        data={users}
        dataKey="getAllHirers.users"
        count={parseInt(count)}
        variables={variables}
        fetchMore={fetchMore}
      >
        {(data) => {
          const showNextLoading = loading && networkStatus === 3 && count !== data.length;

          return (
            <Fragment>
              <StoreContainer>
                {data.map((user) => (
                  <PeopleCard key={user.id} user={user} />
                ))}
              </StoreContainer>

              {showNextLoading && <Loading top="lg" />}
            </Fragment>
          );
        }}
      </InfiniteScroll>
    );
  };

  return (
    <Root maxWidth="md">
      <Head title="Find new People" />

      {renderContent()}
    </Root>
  );
};

export default Store;
