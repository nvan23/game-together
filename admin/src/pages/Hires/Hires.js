import React, { useEffect, useState, Fragment } from 'react';
import { Link, generatePath } from 'react-router-dom';
import styled from 'styled-components';

import { Container } from 'components/Layout';
import Skeleton from 'components/Skeleton';
import Empty from 'components/Empty';
import Head from 'components/Head';

import { timeAgo } from 'utils/date';

import {
  Table,
  Typography,
  Avatar,
  Image,
  Steps,
  Divider,
  Button,
  Tag,
} from 'antd';

import { 
  DownOutlined,
  UpOutlined,
  CheckCircleOutlined,
  SyncOutlined,
  CloseCircleOutlined,
  ClockCircleOutlined,
} from "@ant-design/icons";

import CountDown from 'ant-design-pro/lib/CountDown';

import { GET_ALL_HIRES, GET_ALL_HIRES_TO_WORK_SUBSCRIPTION } from 'graphql/hire';

import { PEOPLE_PAGE_USERS_LIMIT } from 'constants/DataLimit';
import { WAIT, ONGOING, CLOSE } from 'constants/Hire';

import { useQuery } from '@apollo/client';

import { useStore } from 'store';

import * as Routes from 'routes';

import { A } from 'components/Text';

const Root = styled(Container)`
  margin-top: ${(p) => p.theme.spacing.lg};

  @media (min-width: ${(p) => p.theme.screen.lg}) {
    margin-left: ${(p) => p.theme.spacing.lg};
    padding: 0;
  }
`;

const ButtonContainer = styled.div`
  margin-left: 20px;
`

const PeopleContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(180px, 3fr));
  grid-auto-rows: auto;
  grid-gap: 20px;
  margin-bottom: ${(p) => p.theme.spacing.lg};
`;

const Message = styled(Link)`
  text-decoration: none;
  font-size: ${(p) => p.theme.font.size.xs};
`;

const Name = styled.span`
  font-size: ${(p) => p.theme.font.size.xs};
  font-weight: ${(p) => p.theme.font.weight.bold};
  color: ${(p) => p.theme.colors.primary.main};
`;

const ExpandableContainer = styled.div`
  padding: 20px 40px;
`
const StepContainer = styled.div`
  padding: 0 0 10px 30px;
`
const ButtonStepContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin: 20px 0 0 30px;
`

/**
 * Hires management page
 */
const Hires = () => {
  const [{ auth }] = useStore();
  const [current] = useState(1);
  const [isDisabledStartHireButton, setDisabledStartHireButton] = useState(false);
  const { subscribeToMore, data, loading, networkStatus } = useQuery(GET_ALL_HIRES, {
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
  });

  useEffect(() => {
    const unsubscribe = subscribeToMore({
      document: GET_ALL_HIRES_TO_WORK_SUBSCRIPTION,
      variables: { authUserId: auth.user.id },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const newHire = subscriptionData.data.hireCreated;
        const mergedHires = [...prev.getAllHires.hires, newHire];

        return { getAllHires: { hires: mergedHires } };
      },
    });

    return () => {
      unsubscribe();
    };
  }, [auth.user.id, subscribeToMore]);

  const { Text, Title } = Typography;
  const { Step } = Steps;

  const renderContent = () => {
    if (loading && networkStatus === 1) {
      return (
        <PeopleContainer>
          <Skeleton height={280} count={PEOPLE_PAGE_USERS_LIMIT} />
        </PeopleContainer>
      );
    }

    const { hires } = data.getAllHires;
    let countIndex = 1;
    if (!hires.length > 0) return <Empty text="No hire yet." />;

    const dataSourceHireTable = hires.map((hire) => {
      const unitHire = {
        index: countIndex++,
        key: hire.id,
        playerId: hire.hiredPersonId.id,
        avatarPlayer: hire.hiredPersonId.image,
        usernamePlayer: hire.hiredPersonId.username,
        price: hire.price,
        hours: hire.hours,
        cost: hire.cost,
        message: hire.message,
        messageToThank: hire.messageToThank,
        readyClientStatus: hire.readyClientStatus,
        stage: hire.stage,
        status: hire.status,
        createdAt: hire.createdAt,
      }
      return unitHire;
    })

    let uniqueUsernamePlayer = [...new Set(hires.map((hire) => hire.hiredPersonId.username))];
    const usernamePlayerToFilter = uniqueUsernamePlayer.map((usernamePlayer) => {
      const user = {
        text: usernamePlayer,
        value: usernamePlayer,
      }
      return user;
    });

    const stageToFilter = [
      {
        text: 'Wait',
        value: 'wait',
      },
      {
        text: 'Ongoing',
        value: 'ongoing',
      },
      {
        text: 'Close',
        value: 'close',
      },
    ];

    const statusToFilter = [
      {
        text: 'Ongoing',
        value: 'ongoing',
      },
      {
        text: <Text type="success">Success</Text>,
        value: 'success',
      },
      {
        text: <Text type="danger">Fail</Text>,
        value: 'fail',
      },
    ];

    const columns = [
      {
        title: 'Index',
        dataIndex: 'index',
        key: 'index',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.index - b.index,
      },
      {
        title: 'Avatar',
        dataIndex: 'avatarPlayer',
        key: 'avatarPlayer',
        align: 'center',
        render: avatarPlayer => (
          <Avatar
            src={<Image src={avatarPlayer} />}
            size={40}
          />
        )
      },
      {
        title: 'Player',
        dataIndex: 'usernamePlayer',
        key: 'usernamePlayer',
        align: 'center',
        filters: usernamePlayerToFilter,
        onFilter: (value, record) => record.usernamePlayer.indexOf(value) === 0,
        render: usernamePlayer => (
          <A to={generatePath(Routes.USER_PROFILE, { username: usernamePlayer })}>
            <Name>{usernamePlayer}</Name>
          </A>
        )
      },
      {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.price - b.price,
        render: price => (`$${price}`)
      },
      {
        title: 'Hours',
        dataIndex: 'hours',
        key: 'hours',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.hours - b.hours,
      },
      {
        title: 'Cost',
        dataIndex: 'cost',
        key: 'cost',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.cost - b.cost,
        render: cost => (`$${cost}`)
      },
      {
        title: 'Stage',
        dataIndex: 'stage',
        key: 'stage',
        align: 'center',
        filters: stageToFilter,
        onFilter: (value, record) => record.stage.indexOf(value) === 0,
        render: stage => (
          stage === 'close'
            ? <Text>Close</Text>
            : (stage === 'wait'
            ? (<Tag icon={<ClockCircleOutlined />} color="default">Wait</Tag>)
            : (<Tag icon={<SyncOutlined />} color="processing">Ongoing</Tag>))
        )
      },
      {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
        align: 'center',
        filters: statusToFilter,
        onFilter: (value, record) => record.status.indexOf(value) === 0,
        render: status => (
          status === 'success'
          ? (<Tag icon={<CheckCircleOutlined />} color="success">Success</Tag>)
          : (status === 'fail'
            ? (<Tag icon={<CloseCircleOutlined />} color="error">Fail</Tag>)
            : (<Tag icon={<SyncOutlined />} color="processing">Ongoing</Tag>))
        )
      },
      {
        title: 'Created At',
        dataIndex: 'createdAt',
        key: 'createdAt',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => parseInt(a.createdAt) - parseInt(b.createdAt),
        render: createdAt => (
          <>
            {/* for Vietnam, set value in toLocalDateString is "vi-VN": "dd/MM/yyyy" */}
            {`${new Date(parseInt(createdAt)).toLocaleDateString("en-US")} - ${timeAgo(createdAt)} ago`}
          </>
        )
      },
    ];

    const handleCurrentStep = (stage) => {
      switch (stage) {
        case WAIT:
          return 1
        case ONGOING:
          return 2
        case CLOSE:
          return 3
        default:
          return 0
      }
    }

    const handleStartHire = (stage) => {
      if (current < 4) {
        return handleCurrentStep(stage) + 1;
      } else {
        setDisabledStartHireButton(true);
      }
    }

    const targetTime = new Date().getTime() + 3900000;

    let totalHours = 0;
    let totalCost = 0;

    dataSourceHireTable.forEach(({ hours, cost }) => {
      totalHours += hours;
      totalCost += cost
    });

    return (
      <Fragment>
        <Table
          bordered
          size="middle"
          rowKey="key"
          loading={dataSourceHireTable.length > 0 ? false : true}
          columns={columns}
          expandable={{
            expandedRowRender: record => (
              <ExpandableContainer>
                <StepContainer>
                  <Steps current={handleCurrentStep(record.stage)} percent={0} size='small'>
                    <Step
                      title="Ordered"
                      description={
                        <>
                          Find and order hire services on
                          <Message to={generatePath(Routes.STORES)}> Stores.</Message>
                        </>
                      }
                    />
                    <Step title="Wait" description="Wait for players to get ready" />
                    <Step title="Ongoing" description={
                      <>
                        {
                          record.stage === ONGOING
                            ? <CountDown style={{ fontSize: 20 }} target={targetTime} />
                            : 'In process.'
                        }
                      </>
                    }
                    />
                    <Step title="Close" description="End the hiring process at TIME" />
                  </Steps>
                  {
                    !isDisabledStartHireButton &&
                    <ButtonStepContainer>
                      <Button
                        type="primary"
                        onClick={handleStartHire(record.stage)}
                        disabled={isDisabledStartHireButton || !record.readyClientStatus}>
                        Start
                      </Button>
                      <ButtonContainer>
                        <Button
                          type="primary"
                          onClick={handleStartHire(record.stage)}
                          disabled={isDisabledStartHireButton || !record.readyClientStatus}>
                          Close
                      </Button>
                      </ButtonContainer>
                      <ButtonContainer>
                        <Button
                          danger
                          onClick={handleStartHire(record.stage)}
                          disabled={isDisabledStartHireButton}>
                          Report
                      </Button>
                      </ButtonContainer>
                    </ButtonStepContainer>
                  }

                </StepContainer>
                <Divider />
                <Title level={5}>{`HIRE ID: ${record.key.toUpperCase()}`}</Title>
                <p>
                  <Text strong >{` ${auth.user.username}: `}</Text>{record.message}
                </p>
                <p>
                  <Text strong >{` ${record.usernamePlayer}: `}</Text>{record.messageToThank}
                </p>
                <Message to={generatePath(Routes.MESSAGES, { userId: record.playerId })}>Chat more...</Message>
              </ExpandableContainer>
            ),
            rowExpandable: record => record.messageToThank === '' ? false : true,
            expandIcon: ({ expanded, onExpand, record }) =>
              expanded
                ? (<UpOutlined onClick={e => onExpand(record, e)} />)
                : (<DownOutlined onClick={e => onExpand(record, e)} />),
          }}
          dataSource={dataSourceHireTable}
          summary={
            pageData => {
              let totalHoursOnPage = 0;
              let totalCostOnPage = 0;
              let totalCountIndexOnPage = 0;

              pageData.forEach(({ hours, cost }) => {
                totalCountIndexOnPage++;
                totalHoursOnPage += hours;
                totalCostOnPage += cost
              });
              return (
                <>
                  { totalCountIndexOnPage > 10 &&
                    <Table.Summary.Row>
                      <Table.Summary.Cell />
                      <Table.Summary.Cell align='center'>
                        <Text type="danger">{totalCountIndexOnPage}</Text>
                      </Table.Summary.Cell>
                      <Table.Summary.Cell colSpan={3} />
                      <Table.Summary.Cell align='center'>
                        <Text type="danger">{totalHoursOnPage}</Text>
                      </Table.Summary.Cell>
                      <Table.Summary.Cell align='center'>
                        <Text type="danger">${totalCostOnPage}</Text>
                      </Table.Summary.Cell>
                      <Table.Summary.Cell colSpan={2} />
                      <Table.Summary.Cell align='center'>
                        Total / Page
                    </Table.Summary.Cell>
                    </Table.Summary.Row>
                  }
                  <Table.Summary.Row>
                    <Table.Summary.Cell />
                    <Table.Summary.Cell align='center'>
                      <Text type="danger">{countIndex - 1}</Text>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={3} />
                    <Table.Summary.Cell align='center'>
                      <Text type="danger">{totalHours}</Text>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell align='center'>
                      <Text type="danger">${totalCost}</Text>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={2} />
                    <Table.Summary.Cell align='center'>
                      Total
                    </Table.Summary.Cell>
                  </Table.Summary.Row>
                </>
              );
            }}
        />
      </Fragment>
    )
  };

  return (
    <Root maxWidth="md">
      <Head title="My Hires" />

      {renderContent()}
    </Root>
  );
};

export default Hires;
