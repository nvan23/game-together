import React, { useState, Fragment } from 'react';
import { Link, generatePath } from 'react-router-dom';
import styled from 'styled-components';
import { useApolloClient } from '@apollo/client';

import { Container } from 'components/Layout';
import Skeleton from 'components/Skeleton';
import Empty from 'components/Empty';
import Head from 'components/Head';

import { timeAgo } from 'utils/date';

import {
  Table,
  Typography,
  Avatar,
  Image,
  Steps,
  Input,
  Button,
  Space,
  Tag,
} from 'antd';

import Highlighter from 'react-highlight-words';

import {
  DownOutlined,
  UpOutlined,
  CheckOutlined,
  SearchOutlined,
  CheckCircleOutlined,
  SyncOutlined,
  CloseCircleOutlined,
  MinusCircleOutlined,
} from "@ant-design/icons";

import {
  GET_ALL_USERS
} from 'graphql/user';

import { PEOPLE_PAGE_USERS_LIMIT } from 'constants/DataLimit';

import { useQuery } from '@apollo/client';

import { useStore } from 'store';

import * as Routes from 'routes';

import { A } from 'components/Text';

const Root = styled(Container)`
  margin-top: ${(p) => p.theme.spacing.lg};
  max-width: 880px;

  @media (min-width: ${(p) => p.theme.screen.lg}) {
    margin-left: ${(p) => p.theme.spacing.lg};
    padding: 0;
  }
`;

const PeopleContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(180px, 3fr));
  grid-auto-rows: auto;
  grid-gap: 20px;
  margin-bottom: ${(p) => p.theme.spacing.lg};
`;

const Message = styled(Link)`
  text-decoration: none;
  font-size: ${(p) => p.theme.font.size.xs};
`;

const Name = styled.span`
  font-size: ${(p) => p.theme.font.size.xs};
  font-weight: ${(p) => p.theme.font.weight.bold};
  color: ${(p) => p.theme.colors.primary.main};
`;

const ExpandableContainer = styled.div`
  padding: 20px 40px;
`

const People = () => {
  const [{ auth }] = useStore();
  const client = useApolloClient();
  const [searchText, setSearchText] = useState('');
  const [searchedColumn, setSearchedColumn] = useState('');

  const { subscribeToMore, data, loading, networkStatus } = useQuery(GET_ALL_USERS, {
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
  });

  // useEffect(() => {
  //   const unsubscribe = subscribeToMore({
  //     document: GET_ALL_HIRES_TO_WORK_SUBSCRIPTION,
  //     variables: { authUserId: auth.user.id },
  //     updateQuery: (prev, { subscriptionData }) => {
  //       if (!subscriptionData.data) return prev;
  //       const newHireToWork = subscriptionData.data.hireCreated;
  //       const mergedHiresToWork = [...prev.getAllHiresToWork, newHireToWork];

  //       return { getAllHiresToWork: mergedHiresToWork };
  //     },
  //   });

  //   return () => {
  //     unsubscribe();
  //   };
  // }, [auth.user.id, subscribeToMore]);

  const { Text, Title } = Typography;
  const { Step } = Steps;



  const renderContent = () => {
    if (loading && networkStatus === 1) {
      return (
        <PeopleContainer>
          <Skeleton height={280} count={PEOPLE_PAGE_USERS_LIMIT} />
        </PeopleContainer>
      );
    }

    const { users } = data.getAllUsers;

    let countIndex = 1;
    if (!users.length > 0) return <Empty text="No hire yet." />;

    const dataSourceUsersTable = users.map((user) => {
      const unitUser = {
        index: countIndex++,
        key: user.id,
        username: user.username,
        avatar: user.image,
        gender: user.gender,
        price: user.price,
        hours: user.hours,
        completionRate: user.completionRate,
        isBlocked: user.isBlocked,
        isHirer: user.isHirer,
        beingHired: user.beingHired,
        country: user.country,
        createdAt: user.createdAt,
        updatedAt: user.updatedAt,
      }
      return unitUser;
    })

    const genderToFilter = [
      { text: 'Male', value: 'Male' },
      { text: 'Female', value: 'Female' },
      { text: 'Unknown', value: '' },
    ];

    const isHirerToFilter = [
      {
        text: 'Is Hirer',
        value: true,
      },
      {
        text: 'Is not Hirer',
        value: false,
      },
    ];

    const isBlockedToFilter = [
      {
        text: 'Blocked',
        value: true,
      },
      {
        text: 'None',
        value: false,
      },
    ];

    const beingHiredToFilter = [
      {
        text: 'Being Hired',
        value: true,
      },
      {
        text: 'Ready',
        value: false,
      },
    ];

    let handleSearch = (selectedKeys, confirm, dataIndex) => {
      confirm();
      setSearchText(selectedKeys[0]);
      setSearchedColumn(dataIndex);
    };

    let handleReset = clearFilters => {
      clearFilters();
      setSearchText('');
    };


    let getColumnSearchProps = dataIndex => ({
      filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
        <div style={{ padding: 8 }}>
          <Input
            placeholder={`Search ${dataIndex}`}
            value={selectedKeys[0]}
            onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
            onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
            style={{ width: 188, marginBottom: 8, display: 'block' }}
          />
          <Space>
            <Button
              type="primary"
              onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
              icon={<SearchOutlined />}
              size="small"
              style={{ width: 90 }}
            >
              Search
            </Button>
            <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
              Reset
            </Button>
          </Space>
        </div>
      ),
      filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
      onFilter: (value, record) =>
        record[dataIndex]
          ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
          : '',
      render: text =>
        searchedColumn === dataIndex ? (
          <Highlighter
            highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
            searchWords={[searchText]}
            autoEscape
            textToHighlight={text ? text.toString() : ''}
          />
        ) : (
            text
          ),
    });

    const columns = [
      {
        title: 'Index',
        dataIndex: 'index',
        key: 'index',
        align: 'center',
        fixed: 'left',
        width: 50,
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.index - b.index,
      },
      {
        title: 'Avatar',
        dataIndex: 'avatar',
        key: 'avatar',
        align: 'center',
        fixed: 'left',
        width: 80,
        render: avatar => (
          <Avatar
            src={<Image src={avatar} />}
            size={40}
          />
        )
      },
      {
        title: 'Username',
        dataIndex: 'username',
        key: 'username',
        align: 'center',
        fixed: 'left',
        width: 150,
        render: username => (
          <A to={generatePath(Routes.USER_PROFILE, { username: username })}>
            <Name>{username}</Name>
          </A>
        ),
        ...getColumnSearchProps('username'),
      },
      {
        title: 'Gender',
        dataIndex: 'gender',
        key: 'gender',
        width: 80,
        align: 'center',
        filters: genderToFilter,
        onFilter: (value, record) => record.gender.includes(value),
      },
      {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
        align: 'center',
        width: 100,
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.price - b.price,
        render: price => (`$${price}`)
      },
      {
        title: 'Hours',
        dataIndex: 'hours',
        key: 'hours',
        align: 'center',
        width: 80,
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.hours - b.hours,
      },
      {
        title: 'Completion Rate',
        dataIndex: 'completionRate',
        key: 'completionRate',
        align: 'center',
        width: 150,
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.completionRate - b.completionRate,
      },
      {
        title: 'Is Hirer',
        dataIndex: 'isHirer',
        key: 'isHirer',
        align: 'center',
        width: 110,
        filters: isHirerToFilter,
        onFilter: (value, record) => record.isHirer === value,
        render: isHirer => (
          isHirer
            ? <CheckOutlined />
            : ''
        ),
      },
      {
        title: 'Being Hired',
        dataIndex: 'beingHired',
        key: 'beingHired',
        align: 'center',
        width: 150,
        filters: beingHiredToFilter,
        onFilter: (value, record) => record.beingHired === value,
        render: beingHired => (
          beingHired
            ? (<Tag icon={<SyncOutlined spin />} color="processing">Being Hired</Tag>)
            : (<Tag icon={<CheckCircleOutlined />} color="success">Ready</Tag>)
        ),
      },
      {
        title: 'Block',
        dataIndex: 'isBlocked',
        key: 'isBlocked',
        align: 'center',
        width: 100,
        filters: isBlockedToFilter,
        onFilter: (value, record) => record.isBlocked === value,
        render: isBlocked => (
          isBlocked
            ? (<Tag icon={<MinusCircleOutlined />} color="error">Blocked</Tag>)
            : (<Tag icon={<SyncOutlined />} color="success">Active</Tag>)
        ),
      },
      {
        title: 'Country',
        dataIndex: 'country',
        key: 'country',
        align: 'center',
        width: 150,
        ...getColumnSearchProps('country'),
      },
      {
        title: 'Created At',
        dataIndex: 'createdAt',
        key: 'createdAt',
        align: 'center',
        width: 200,
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => parseInt(a.createdAt) - parseInt(b.createdAt),
        render: createdAt => (
          <>
            {/* for Vietnam, set value in toLocalDateString is "vi-VN": "dd/MM/yyyy" */}
            {createdAt
              ? `${timeAgo(createdAt)} ago (${new Date(parseInt(createdAt)).toLocaleDateString("en-US")})`
              : 'Unknown'
            }
          </>
        )
      },
      {
        title: 'Updated At',
        dataIndex: 'updatedAt',
        key: 'updatedAt',
        align: 'center',
        width: 200,
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => parseInt(a.createdAt) - parseInt(b.createdAt),
        render: createdAt => (
          <>
            {/* for Vietnam, set value in toLocalDateString is "vi-VN": "dd/MM/yyyy" */}
            {createdAt
              ? `${timeAgo(createdAt)} ago (${new Date(parseInt(createdAt)).toLocaleDateString("en-US")})`
              : 'Unknown'
            }
          </>
        )
      },
      {
        title: 'Action',
        dataIndex: 'key',
        key: 'key',
        align: 'center',
        fixed: 'right',
        width: 100,
        render: (key) => (
          <a>Block</a>
        )
      },
    ];

    let totalHours = 0;
    let totalPrice = 0;

    dataSourceUsersTable.forEach(({ hours, price }) => {
      totalHours += hours;
      totalPrice += price;
    });

    return (
      <Fragment>
        <Table
          bordered
          size="middle"
          rowKey="key"
          loading={dataSourceUsersTable.length > 0 ? false : true}
          columns={columns}
          scroll={{ x: 1800 }}
          expandable={{
            expandedRowRender: record => (
              <ExpandableContainer>
                <Message to={generatePath(Routes.MESSAGES, { userId: record.key })}>Chat more...</Message>
              </ExpandableContainer>
            ),
            rowExpandable: record => record.message === '' ? false : true,
            expandIcon: ({ expanded, onExpand, record }) =>
              expanded
                ? (<UpOutlined onClick={e => onExpand(record, e)} />)
                : (<DownOutlined onClick={e => onExpand(record, e)} />),
          }}
          dataSource={dataSourceUsersTable}
          summary={
            pageData => {
              let totalHoursOnPage = 0;
              let totalPriceOnPage = 0;
              let totalCountIndexOnPage = 0;

              pageData.forEach(({ hours, price }) => {
                totalCountIndexOnPage++;
                totalHoursOnPage += hours;
                totalPriceOnPage += price;
              });
              return (
                <>
                  { totalCountIndexOnPage >= 10 &&
                    <Table.Summary.Row>
                      <Table.Summary.Cell />
                      <Table.Summary.Cell align='center'>
                        <Text type="danger">{totalCountIndexOnPage}</Text>
                      </Table.Summary.Cell>
                      <Table.Summary.Cell colSpan={3} />
                      <Table.Summary.Cell align='center'>
                        <Text type="danger">${totalPriceOnPage}</Text>
                      </Table.Summary.Cell>
                      <Table.Summary.Cell align='center'>
                        <Text type="danger">{totalHoursOnPage}</Text>
                      </Table.Summary.Cell>
                      <Table.Summary.Cell colSpan={7} />
                      <Table.Summary.Cell align='center'>
                        Total / Page
                    </Table.Summary.Cell>
                    </Table.Summary.Row>
                  }
                  <Table.Summary.Row>
                    <Table.Summary.Cell />
                    <Table.Summary.Cell align='center'>
                      <Text type="danger">{countIndex - 1}</Text>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={3} />
                    <Table.Summary.Cell align='center'>
                      <Text type="danger">${totalPrice}</Text>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell align='center'>
                      <Text type="danger">{totalHours}</Text>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={7} />
                    <Table.Summary.Cell align='center'>
                      Total
                    </Table.Summary.Cell>
                  </Table.Summary.Row>
                </>
              );
            }}
        />
      </Fragment>
    )
  };

  return (
    <Root maxWidth="md">
      <Head title="Workplace" />

      {renderContent()}
    </Root>
  );
};

export default People;
