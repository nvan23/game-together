import { gql } from '@apollo/client';

const User = `
  id
  username
  image
`

const hiresPayload = `
  id
  clientId {
    ${User}
  }
  hiredPersonId {
    ${User}
  }
  price
  hours
  cost
  message
  messageToThank
  readyClientStatus
  stage
  status
  createdAt
`

/**
 * Get my all hires
 */
export const GET_ALL_HIRES = gql`
  query {
    getAllHires {
      count
      hires {
        ${hiresPayload}
      }
    }
  }
`;

const ReportPayload = `
  id
  hireId {
    ${hiresPayload}
  }
  clientId {
    ${User}
  }
  responded
  refundRequest
  content
  createdAt
`

export const GET_ALL_REPORTS = gql`
  query {
    getAllReports {
      id
      hireId {
        id
        price
        hours
        cost
        status
        createdAt
      }
      clientId {
        id
        username
        image
      }
      responded
      refundRequest
      content
      createdAt
    }
  }
`;

/**
 * Get all hires of client
 */
export const GET_ALL_HIRES_TO_WORK = gql`
  query {
    getAllHiresToWork {
      ${hiresPayload}
    }
  }
`;

/**
 * Get a hire
 */
export const GET_HIRE = gql`
  query($hireId: String!) {
    getHire(hireId: $hireId) {
      ${hiresPayload}
    }
  }
`;

/**
 * Creates a hire relationship between two users
 */
export const CREATE_HIRE = gql`
  mutation($input: CreateHireInput!) {
    createHire(input: $input) {
      id
    }
  }
`;

/**
 * Creates a hire relationship between two users
 */
export const CHANGE_READY_CLIENT_STATUS = gql`
  mutation($input: ChangeReadyClientStatusInput) {
    changeReadyClientStatus(input: $input) {
      ${hiresPayload}
    }
  }
`;

export const REFUND_REQUEST_TO_LESSOR = gql`
  mutation($hireId: ID!) {
    sendRefundRequestToLessor(input: $hireId) {
      ${hiresPayload}
    }
  }
`;
/**
 * Close a hire relationship between two users
 */
export const CLOSE_HIRE = gql`
  mutation($input: CloseHireInput!) {
    closeHire(input: $input) {
      id
    }
  }
`;

/**
 * Report a hire relationship between two users
 */
export const REPORT_HIRE = gql`
  mutation($input: ReportHireInput!) {
    reportHire(input: $input) {
      id
    }
  }
`;

/**
 * Get my all hires in real time
 */
export const GET_ALL_HIRES_TO_WORK_SUBSCRIPTION = gql`
  subscription($authUserId: ID!) {
    hireCreated(authUserId: $authUserId) {
      ${hiresPayload}
    }
  }
`;
