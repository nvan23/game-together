import Admin from './Admin';
import User from './User';
import Post from './Post';
import Like from './Like';
import Follow from './Follow';
import Hire from './Hire';
import TimeoutContainer from './TimeoutContainer';
import Game from './Game';
import Donate from './Donate';
import Feedback from './Feedback';
import Report from './Report';
import Comment from './Comment';
import Notification from './Notification';
import Message from './Message';

export default {
  Admin,
  User,
  Post,
  Like,
  Follow,
  Hire,
  TimeoutContainer,
  Game,
  Donate,
  Feedback,
  Report,
  Comment,
  Notification,
  Message,
};
