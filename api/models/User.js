import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';

const Schema = mongoose.Schema;

/**
 * User schema that has references to Post, Like, Comment, Follow and Notification schemas
 */
const userSchema = new Schema(
  {
    fullName: {
      type: String,
      required: true,
    },
    gender: String,
    isBlocked: {
      type: Boolean,
      default: false,
    },
    isHirer: {
      type: Boolean,
      default: false,
    },
    beingHired: {
      type: Boolean,
      default: false,
    },
    email: {
      type: String,
      required: true,
      lowercase: true,
      trim: true,
      unique: true,
    },
    username: {
      type: String,
      required: true,
      lowercase: true,
      trim: true,
      unique: true,
    },
    passwordResetToken: String,
    passwordResetTokenExpiry: Date,
    password: {
      type: String,
      required: true,
    },
    image: String,
    imagePublicId: String,
    coverImage: String,
    coverImagePublicId: String,
    country: String,
    description: String,
    balance: {
      type: Number,
      min: 0,
    },
    price: {
      type: Number,
      min: 0,
      max: 500000,
    },
    isOnline: {
      type: Boolean,
      default: false,
    },
    hours: Number,
    completionRate: Number,
    messageToThank: String,
    hiredMe: {
      type: Boolean,
      default: false,
    },
    posts: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Post',
      },
    ],
    likes: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Like',
      },
    ],
    comments: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Comment',
      },
    ],
    followers: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Follow',
      },
    ],
    following: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Follow',
      },
    ],
    games: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Game',
      },
    ],
    otherGames: [],
    donate: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Donate',
      },
    ],
    donated: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Donate',
      },
    ],
    feedbacks: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Feedback',
      },
    ],
    hiredPersons: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Hire',
      },
    ],
    hirers: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Hire',
      },
    ],
    hired: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Hire',
      },
    ],
    hiring: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Hire',
      }
    ],
    notifications: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Notification',
      },
    ],
    messages: [
      {
        type: Schema.Types.ObjectId,
        ref: 'User',
      },
    ],
  },
  {
    timestamps: true,
  }
);

/**
 * Hashes the users password when saving it to DB
 */
userSchema.pre('save', function (next) {
  if (!this.isModified('password')) {
    return next();
  }

  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next(err);

    bcrypt.hash(this.password, salt, (err, hash) => {
      if (err) return next(err);

      this.password = hash;
      next();
    });
  });
});

export default mongoose.model('User', userSchema);
