import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
 * Game schema
 */
const gameSchema = Schema(
  {
    name: {
      type: String,
      unique: true,
    },
  },
  {
    timestamps: true,
  }
);

export default mongoose.model('Game', gameSchema);
