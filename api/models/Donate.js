import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
 * Donate schema that has references to User schema
 */
const donateSchema = Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    donatedPerson: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    amountOfMoney: {
      type: Number,
      require: true,
    },
    message: String,
  },
  {
    timestamps: true,
  }
);

export default mongoose.model('Donate', donateSchema);
