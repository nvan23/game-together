import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
 * Feedback schema that has references to User schema
 */
const feedbackSchema = Schema(
  {
    uuid: String,
    content: String,
    hours: {
      type: Number,
    },
    star: {
      type: Number,
    },
  },
  {
    timestamps: true,
  }
);

export default mongoose.model('Feedback', feedbackSchema);
