import mongoose from 'mongoose';
import { WAIT, ONGOING, CLOSE, SUCCESS, FAILURE } from '../constants/Hire';

const Schema = mongoose.Schema;

/**
 * Hire schema that has references to User schema
 */
const hireSchema = Schema(
  {
    clientId: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    hiredPersonId: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    feedbackId: {
      type: Schema.Types.ObjectId,
      ref: 'Feedback',
    },
    reportId: {
      type: Schema.Types.ObjectId,
      ref: 'Report',
    },
    messages: [
      {
        type: Schema.Types.ObjectId,
        ref: 'User',
      },
    ],
    message: String,
    messageToThank: String,
    price: {
      type: Number,
      require: true,
    },
    hours: {
      type: Number,
      require: true,
      min: 1,
      max: 24,
    },
    timeout: Number,
    cost: {
      type: Number,
      min: 0,
    },
    // stage: {
    //   type: String,
    //   enum: [WAIT, ONGOING, CLOSE],
    //   default: WAIT,
    // },
    stage: {
      type: String,
    },
    readyClientStatus: {
      type: Boolean,
      default: false,
    },
    refundRequestStatus: Boolean,
    feedbackStatus: Boolean,
    // status: {
    //   type: String,
    //   enum: [ONGOING, SUCCESS, FAILURE],
    //   default: ONGOING,
    // },
    status: {
      type: String,
    },
    accomplished: {
      type: Boolean,
      default: false,
    },
    beReported: {
      type: Boolean,
      default: false,
    },
    feedback: {
      type: Schema.Types.ObjectId,
      ref: 'Feedback',
    }
  },
  {
    timestamps: true,
  }
);

export default mongoose.model('Hire', hireSchema);
