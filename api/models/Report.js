import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
 * Report schema
 */
const reportSchema = Schema(
  {
    hireId: {
      type: Schema.Types.ObjectId,
      ref: 'Hire',
    },
    clientId: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    refundRequest: Boolean,
    relyRefundRequest: String,
    responded: Boolean,
    content: String,
  },
  {
    timestamps: true,
  }
);

export default mongoose.model('Report', reportSchema);
