import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
 * Timeout Container schema
 */
const timeoutContainerSchema = Schema(
  {
    key: {
      type: Schema.Types.ObjectId,
      ref: 'Hire',
    },
    value: String,
  },
  {
    timestamps: true,
  }
);

export default mongoose.model('TimeoutContainer', timeoutContainerSchema);
