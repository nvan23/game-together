import adminResolver from './admin';
import userResolver from './user';
import postResolver from './post';
import likeResolver from './like';
import followResolver from './follow';
import hireResolver from './hire';
import timeoutcontainerResolver from './timeoutcontainer';
import gameResolver from './game';
import donateResolver from './donate';
import feedbackResolver from './feedback';
import reportResolver from './report';
import commentResolver from './comment';
import notificationResolver from './notification';
import message from './message';

export default [
  adminResolver,
  userResolver,
  postResolver,
  likeResolver,
  followResolver,
  hireResolver,
  timeoutcontainerResolver,
  gameResolver,
  donateResolver,
  feedbackResolver,
  reportResolver,
  commentResolver,
  notificationResolver,
  message,
];
