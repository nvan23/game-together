import {
  timeoutContainer,
  clearTimeoutContainer,
} from '../utils/time';

const Query = {
  /**
   * Gets my all timeout container
   */
  getAllTimeoutContainers: async (root, args, { TimeoutContainer }) => {
    const count = await TimeoutContainer.countDocuments();
    const timeoutContainers = await TimeoutContainer.find().sort({ createdAt: 'desc' });
    return { timeoutContainers, count };
  },

  /**
   * Gets a timeout container
   * @param {string} key key of timeout container dictionary
   */
  getTimeoutContainer: async (root, { key }, { TimeoutContainer }) => {
    const timeoutContainer = await TimeoutContainer.findOne({ key: key })

    if (!timeoutContainer) {
      throw new Error("User with given params doesn't exists.");
    }

    return timeoutContainer;
  },

};

const Mutation = {
  /**
   * Creates a hire relationship between users
   */
  backup: async (root, args, { TimeoutContainer }) => {

    if (Object.keys(timeoutContainer).length === 0) {
      return;
    }

    Object.entries(timeoutContainer).forEach(async ([key, value]) => {
      await new TimeoutContainer({
        key: key,
        value: value,
      }).save();
    });

    let count = await TimeoutContainer.countDocuments();
    count = count + parseInt(Object.keys(timeoutContainer).length);

    clearTimeoutContainer();

    const timeoutContainers = await TimeoutContainer.find().sort({ createdAt: 'desc' });

    return { timeoutContainers, count, timeoutContainer };
  }
};


export default { Query, Mutation };
