const Query = {
  /**
   * Gets all games
   */
  getAllGames: async (root, args, { Game }) => {
    const games = await Game.find();
    return games;
  },

  /**
   * Gets a game
   * @param {string} id game id
   */
  getGame: async (root, { id }, { Game }) => {
    const game = await Game.find({ _id: id });
    return game;
  },

};

const Mutation = {
  /**
   * Creates a game
   *
   * @param {string} name game name
   */
  createGame: async (root, { input: { name } }, { Game }) => {
    if (name === '') {
      throw new Error('Game name cannot be empty');
    }

    const game = await new Game({
      name: name,
    }).save();

    return game;
  },

  /**
   * Delete a game
   *
   * @param {string} id game id
   */
  deleteGame: async (root, { input: { id } }, { Game }) => {
    // Find game and remove it
    const game = await Game.findByIdAndRemove(id);
    return game;
  },

  /**
   * Rename a game
   *
   * @param {string} id game id
   * @param {string} name game name to rename
   */
  renameGame: async (root, { input: { id, name } }, { Game }) => {
    const game = await Game.findOneAndUpdate({ _id: id }, { name: name });
    return game;
  },
};

export default { Query, Mutation };
