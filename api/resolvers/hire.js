import mongoose from 'mongoose';
import { CLOSE, PRICE_DEFAULT, SUCCESS } from '../constants/Hire';
import { WAIT, ONGOING } from '../constants/Hire';
import { pubSub } from '../utils/apollo-server';

import {
  computeCost,
  computeClientBalance,
  computePlayerBalance
} from '../utils/balance';

import {
  timeoutContainer,
  setTimeoutContainer,
  setIntervalContainer,
  addHireToTimeoutContainer,
  addHireIntoSetTimeoutContainer,
  addHireIntoSetIntervalContainer,
  clearTimeoutHireFromSetIntervalContainer,
  clearTimeoutHireFromSetTimeoutContainer,
} from '../utils/time';

import {
  HIRE_CREATED,
  CHANGE_READY_CLIENT_STATUS_SUBSCRIPTION,
  CHANGE_HIRE_STAGE,
  OVER_TIME_OUT_SUBSCRIPTION,
  REFUND_REQUEST_TO_LESSOR_SUBSCRIPTION,
} from '../constants/Subscriptions';

const Query = {
  /**
   * Gets my all hires
   */
  getAllHires: async (root, args, { Hire, authUser }) => {
    const count = await Hire.countDocuments({ clientId: authUser.id });
    const hires = await Hire.find({ clientId: authUser.id })
      .populate({
        path: 'clientId',
        select: 'id username image',
      })
      .populate({
        path: 'hiredPersonId',
        select: 'id username image',
      })
      .populate({
        path: 'feedbackId',
        select: 'id content star',
      })
      .populate({
        path: 'reportId',
        select: 'id refundRequest responded relyRefundRequest',
      })
      .sort({ createdAt: 'desc' });

    hires.forEach(hire => {
      Object.entries(timeoutContainer).forEach(async ([key, value]) => {
        if (hire.id.toString() === key) {
          if (parseInt(hire.timeout) < parseInt(value)) {
            hire.timeout = value
            return hire
          }
        }
      });
    })

    return { hires, count };
  },

  /**
   * Gets all hires of client
   */
  getAllHiresToWork: async (root, args, { Hire, authUser }) => {
    const hires = await Hire.find({ hiredPersonId: authUser.id })
      .populate({
        path: 'clientId',
        select: 'id username image',
      })
      .populate({
        path: 'hiredPersonId',
        select: 'id username image',
      })
      .populate({
        path: 'feedbackId',
        select: 'id content star',
      })
      .populate({
        path: 'reportId',
        select: 'id refundRequest responded relyRefundRequest',
      })
      .sort({ createdAt: 'desc' });

    hires.forEach(hire => {
      Object.entries(timeoutContainer).forEach(async ([key, value]) => {
        if (hire.id.toString() === key) {
          if (parseInt(hire.timeout) < parseInt(value)) {
            hire.timeout = value
            return hire
          }
        }
      });
    })
    return hires;
  },

  /**
   * Gets a hire
   * @param {string} userId user id
   * @param {string} hireId hire id
   */
  getHire: async (root, { hireId }, { Hire, TimeoutContainer }) => {
    const hire = await Hire.findOne({ _id: hireId })
      .populate({
        path: 'clientId',
        select: 'id username image',
      })
      .populate({
        path: 'hiredPersonId',
        select: 'id username image',
      });

    const timeout = timeoutContainer.hasOwnProperty('hireId')
      ? (parseInt(timeoutContainer[hireId]) > parseInt(hire.timeout)
        ? timeoutContainer[hireId]
        : hire.timeout
      )
      : hire.timeout;

    return { timeout, hire };
  },

};

const Mutation = {
  /**
   * Creates a hire relationship between users
   *
   * @param {string} clientId client id
   * @param {string} hiredPersonId hired person id 
   */
  createHire: async (root, { input: { clientId, hiredPersonId, hours, message } }, { Hire, User }) => {

    const user = await User.findOne({ _id: clientId });
    const hiredPerson = await User.findOne({ _id: hiredPersonId });
    let messageToThank = `Hello ${user.fullName}, I'm ${hiredPerson.fullName}. Thank you for hiring me.`

    if (typeof hours !== 'number') {
      throw new Error('Input Error');
    }

    if (hours < 0 || hours > 24) {
      throw new Error('Rental service is not available');
    }

    if (message === '') {
      message = `Hello ${hiredPerson.fullName}, I'm ${user.fullName}. I just hired a friend to play games together for ${hours} hours. Check it out and let me know when to start.`
    } else {
      message = `Hello ${hiredPerson.fullName}, I'm ${user.fullName}. `.concat(message)
    }

    if (hiredPerson.messageToThank !== '') {
      messageToThank = hiredPerson.messageToThank
    }

    if (!hiredPerson.hiredMe) {
      throw new Error('Users do not provide rental services, please come back another time.')
    }

    if (hiredPerson.price < PRICE_DEFAULT) {
      throw new Error('Rental service is not available');
    }

    if (parseInt(user.balance) < parseInt(hiredPerson.price) * parseInt(hours)) {
      throw new Error('Your balance is not enough to pay')
    }

    let hire = await new Hire({
      clientId: clientId,
      hiredPersonId: hiredPersonId,
      timeout: 0,
      feedbackId: mongoose.Types.ObjectId("5ffda688d10e4235d8dc3a1c"),
      reportId: mongoose.Types.ObjectId("5ffda688d10e4235d8dc3a1c"),
      feedbackStatus: false,
      beReported: false,
      refundRequestStatus: false,
      price: hiredPerson.price,
      hours: parseInt(hours),
      cost: computeCost(hiredPerson.price, hours),
      message: message,
      messageToThank: messageToThank,
      stage: WAIT,
      status: ONGOING,
      readyClientStatus: false,
      createdAt: new Date().toISOString(),
    }).save();

    hire = await hire
      .populate({
        path: 'clientId',
        select: 'id username image',
      })
      .populate({
        path: 'hiredPersonId',
        select: 'id username image',
      })
      .populate({
        path: 'feedbackId',
        select: 'id content star',
      })
      .populate({
        path: 'reportId',
        select: 'id refundRequest responded relyRefundRequest',
      })
      .execPopulate();

    pubSub.publish(HIRE_CREATED, { hireCreated: hire });

    // const callback = async () => {
    //   await User.findOneAndUpdate(
    //     { _id: hiredPersonId },
    //     {
    //       balance: computePlayerBalance(hiredPerson.balance, hiredPerson.price, hours)
    //     },
    //     {
    //       new: true
    //     });
    // }

    // Compute balance of client and  player   
    await User.findOneAndUpdate(
      { _id: clientId },
      {
        balance: computeClientBalance(user.balance, hiredPerson.price, hours),
      },
      {
        new: true
      },
    );

    return hire;
  },

  /**
   * Change stage of the hire relationship between users
   *
   * @param {string} hireId hire id
   * @param {string} clientId client id
   * @param {string} stage client change stage of the hire
   */
  changeHireState: async (root, { input: { hireId, stage } }, { Hire, authUser }) => {
    const hire = await Hire.findOne({ _id: hireId })
      .populate({
        path: 'clientId',
        select: 'id username image',
      })
      .populate({
        path: 'hiredPersonId',
        select: 'id username image',
      })
      .populate({
        path: 'feedbackId',
        select: 'id content star',
      })
      .populate({
        path: 'reportId',
        select: 'id refundRequest responded relyRefundRequest',
      });

    if (parseInt(hire.hours) <= 0) {
      throw new Error('Hours Input Error');
    }

    if (hire.clientId.id.toString() !== authUser.id.toString()) {
      throw new Error('Action not be granted');
    }
    if (hire.stage === CLOSE) {
      return hire;
    }

    if (hire.stage === ONGOING) {
      if (stage === CLOSE) {
        let closeHire = await Hire.findOneAndUpdate(
          { _id: hireId, clientId: authUser.id },
          {
            stage: CLOSE,
            timeout: parseInt(hire.hours) * 3600000,
          }
        );
        closeHire = await closeHire
          .populate({
            path: 'clientId',
            select: 'id username image',
          })
          .populate({
            path: 'hiredPersonId',
            select: 'id username image',
          })
          .populate({
            path: 'feedbackId',
            select: 'id content star',
          })
          .populate({
            path: 'reportId',
            select: 'id refundRequest responded relyRefundRequest',
          })
          .execPopulate();

        pubSub.publish(
          CHANGE_HIRE_STAGE,
          { stageChangedSubscription: closeHire }
        );
        return closeHire;
      }
    }

    if (stage === WAIT) {
      return hire;
    }

    if (stage === CLOSE) {
      throw new Error('Can not skip ONGOING step')
    }

    if (stage === ONGOING) {
      let setTimeoutHire = setTimeout(async function () {
        clearInterval(setIntervalContainer[hireId]);
        clearTimeoutHireFromSetIntervalContainer(hireId, setIntervalHire);
        clearTimeoutHireFromSetTimeoutContainer(hireId, setTimeoutHire);
        let overHireTimeout = await Hire.findOneAndUpdate(
          { _id: hireId, clientId: authUser.id },
          {
            stage: CLOSE,
            status: SUCCESS,
            timeout: parseInt(hire.hours) * 3600000,
          }
        );
        overHireTimeout = await overHireTimeout
          .populate({
            path: 'clientId',
            select: 'id username image',
          })
          .populate({
            path: 'hiredPersonId',
            select: 'id username image',
          })
          .populate({
            path: 'feedbackId',
            select: 'id content star',
          })
          .populate({
            path: 'reportId',
            select: 'id refundRequest responded relyRefundRequest',
          })
          .execPopulate();

        pubSub.publish(
          OVER_TIME_OUT_SUBSCRIPTION,
          { overTimeoutSubscription: overHireTimeout }
        );
      }, 30 * 1000);

      addHireToTimeoutContainer(hireId, 1)

      addHireIntoSetTimeoutContainer(hireId, setTimeoutHire)
      let setIntervalHire = setInterval(function () {
        if (parseInt(timeoutContainer[hireId]) <= parseInt(hire.hours) * 3600) {
          timeoutContainer[hireId] = parseInt(timeoutContainer[hireId]) + 1
        }
        return setTimeoutContainer;
      }, 1000);

      addHireIntoSetIntervalContainer(hireId, setIntervalHire)

      let hireChanged = await Hire.findOneAndUpdate(
        { _id: hireId, clientId: authUser.id },
        { stage: ONGOING }
      );

      hireChanged = await hireChanged
        .populate({
          path: 'clientId',
          select: 'id username image',
        })
        .populate({
          path: 'hiredPersonId',
          select: 'id username image',
        })
        .populate({
          path: 'feedbackId',
          select: 'id content star',
        })
        .populate({
          path: 'reportId',
          select: 'id refundRequest responded relyRefundRequest',
        })
        .execPopulate();

      pubSub.publish(
        CHANGE_HIRE_STAGE,
        { stageChangedSubscription: hireChanged }
      );
    }
    return hire;
  },

  clearTimeoutHire: async (root, { input: { hireId } }, { }) => {
    clearInterval(setIntervalContainer[hireId]);
    clearTimeout(setTimeoutContainer[hireId]);
    const perform = `Clear timeout of hire id (${hireId} successfully.)`;
    return perform;
  },

  /**
   * Change status of client into ready
   */
  changeReadyClientStatus: async (root, { input: { hireId } }, { Hire, authUser }) => {
    let hire = await Hire.findOneAndUpdate(
      { _id: hireId, hiredPersonId: authUser.id },
      { readyClientStatus: true },
      {
        new: true
      })
      .populate({
        path: 'clientId',
        select: 'id username image',
      })
      .populate({
        path: 'hiredPersonId',
        select: 'id username image',
      })
      .populate({
        path: 'reportId',
        select: 'id refundRequest responded relyRefundRequest',
      })
      .populate({
        path: 'feedbackId',
        select: 'id content star',
      });

    hire = await hire
      .populate({
        path: 'clientId',
        select: 'id username image',
      })
      .populate({
        path: 'hiredPersonId',
        select: 'id username image',
      })
      .populate({
        path: 'reportId',
        select: 'id refundRequest responded relyRefundRequest',
      })
      .populate({
        path: 'feedbackId',
        select: 'id content star',
      })
      .execPopulate();

    pubSub.publish(
      CHANGE_READY_CLIENT_STATUS_SUBSCRIPTION,
      { changeReadyClientStatusSubscription: hire }
    );

    return hire;
  },

  replyRefundRequest: async (root, { clientId, hiredPersonId, hireId, reportId, value, hours }, { Hire, User, Report }) => {
    const user = await User.findOne({ _id: clientId });
    const hiredPerson = await User.findOne({ _id: hiredPersonId });

    if (!value) {
      let report = await Report.findOneAndUpdate(
        { _id: reportId, },
        {
          relyRefundRequest: 'no',
        },
        {
          new: true
        });

      report = await report
        .populate({
          path: 'hireId',
          select: 'id price hours cost status createdAt',
          populate: [
            { path: 'clientId' },
            { path: 'hiredPersonId' },
          ],
        })
        .populate({
          path: 'clientId',
          select: 'id username image'
        })
        .execPopulate();

      let hire = await Hire.findOneAndUpdate(
        { _id: hireId, },
        {
          refundRequestStatus: value,
        },
        {
          new: true
        })

      hire = await hire
        .populate({
          path: 'clientId',
          select: 'id username image',
        })
        .populate({
          path: 'hiredPersonId',
          select: 'id username image',
        })
        .populate({
          path: 'feedbackId',
          select: 'id content star',
        })
        .populate({
          path: 'reportId',
          select: 'id refundRequest responded relyRefundRequest',
        })
        .execPopulate();

      pubSub.publish(
        REFUND_REQUEST_TO_LESSOR_SUBSCRIPTION,
        { sendRefundRequestToLessorSubscription: hire }
      );

      return hire;
    }

    let hire = await Hire.findOneAndUpdate(
      { _id: hireId, },
      {
        refundRequestStatus: value,
        relyRefundRequest: 'yes',
        balance: computePlayerBalance(user.balance, hiredPerson.price, hours),
      },
      {
        new: true
      })
      .populate({
        path: 'clientId',
        select: 'id username image',
      })
      .populate({
        path: 'hiredPersonId',
        select: 'id username image',
      })
      .populate({
        path: 'reportId',
        select: 'id refundRequest responded relyRefundRequest',
      })
      .populate({
        path: 'feedbackId',
        select: 'id content star',
      });

    hire = await hire
      .populate({
        path: 'clientId',
        select: 'id username image',
      })
      .populate({
        path: 'hiredPersonId',
        select: 'id username image',
      })
      .populate({
        path: 'feedbackId',
        select: 'id content star',
      })
      .populate({
        path: 'reportId',
        select: 'id refundRequest responded relyRefundRequest',
      })
      .execPopulate();

    pubSub.publish(
      REFUND_REQUEST_TO_LESSOR_SUBSCRIPTION,
      { sendRefundRequestToLessorSubscription: hire }
    );

    return hire;
  },

  /**
   * Close a hire relationship between users
   *
   * @param {string} id hire id
   * @param {number} starValue client give star after completing a hire
   * @param {string} feedback client give feedback after completing a hire
   */
  closeHire: async (root, { input: { id, starValue, feedback } }, { Hire }) => {
    // Push accomplish status, star value and feedback to hire collection
    await Hire.findOneAndUpdate({ _id: id }, { $push: { accomplished: true, star: starValue, feedback: feedback } });

    return hire;
  },

  /**
   * Report a hire
   *
   * @param {string} id hire id
   * @param {string} comment client give comment about hire 
   */
  reportHire: async (root, { input: { id, comment } }, { Hire }) => {
    const hire = await Hire.findOneAndUpdate({ _id: id }, { beReported: true });
    return hire;
  },

  backupAllTimeoutHires: async (root, args, { Hire }) => {
    const count = await Hire.countDocuments();
    const hires = await Hire.find();

    hires.forEach(hire => {
      Object.entries(timeoutContainer).forEach(async ([key, value]) => {
        if (key === hire.id) {
          if (parseInt(hire.timeout) < parseInt(value)) {
            hire.timeout = value;
          }
        }
      });
    })

    return { hires, count };
  },

};

const Subscription = {
  /**
   * Subscribes to my hire created event
   */
  hireCreated: {
    subscribe: () => pubSub.asyncIterator('HIRE_CREATED')
    // subscribe: withFilter(
    //   () => pubSub.asyncIterator(HIRE_CREATED),
    //   (payload, variables) => {
    //     const { clientId, hiredPersonId } = payload.hireCreated;
    //     const { authUserId } = variables;

    //     const isAuthUserClientOrPlayer = authUserId === clientId;
    //     const isUserClientOrPlayer =  authUserId === hiredPersonId;

    //     return isAuthUserClientOrPlayer && isUserClientOrPlayer;
    //   }
    // ),
  },

  changeReadyClientStatusSubscription: {
    subscribe: () => pubSub.asyncIterator('CHANGE_READY_CLIENT_STATUS_SUBSCRIPTION')
  },

  sendRefundRequestToLessorSubscription: {
    subscribe: () => pubSub.asyncIterator('REFUND_REQUEST_TO_LESSOR_SUBSCRIPTION')
  },


  stageChangedSubscription: {
    subscribe: () => pubSub.asyncIterator('CHANGE_HIRE_STAGE')
  },

  overTimeoutSubscription: {
    subscribe: () => pubSub.asyncIterator('OVER_TIME_OUT_SUBSCRIPTION')
  }
};

export default { Query, Mutation, Subscription };
