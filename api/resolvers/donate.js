const Query = {
  /**
   *  Gets all donates
   */
  getAllDonates: async (root, args, { Donate }) => {
    const donates = await Donate.find();
    return donates;
  },

  /**
   *  Gets all donates of player
   * @param {string} userId user id
   */
  getDonates: async (root, { userId }, { Donate }) => {
    const donates = await Donate.find({ user: userId });
    return donates;
  },

  /**
   *  Gets a donate of player
   * @param {string} id user id
   */
  getDonates: async (root, { id }, { Donate }) => {
    const donates = await Donate.find({ _id: id });
    return donates;
  },

};

export default { Query };