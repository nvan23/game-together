import { pubSub } from '../utils/apollo-server';
import uuid from 'uuid/dist/v4';

import {
  REPORT_CREATED,
  REFUND_REQUEST_TO_LESSOR_SUBSCRIPTION,
} from '../constants/Subscriptions';

const Query = {

  getAllReports: async (root, args, { Report }) => {
    const reports = await Report.find()
      .populate({
        path: 'hireId',
        select: 'id price hours cost status createdAt',
        populate: [
          { path: 'clientId' },
          { path: 'hiredPersonId' },
        ],
      })
      .populate({
        path: 'clientId',
        select: 'id username image'
      })
      .sort({ createdAt: 'desc' });
    return reports;
  },

};

const Mutation = {
  createReportClient: async (root, { input: { hireId, clientId, content, refundRequest } }, { Report, Hire }) => {
    let report = await new Report({
      hireId: hireId,
      clientId: clientId,
      responded: false,
      refundRequest: refundRequest,
      relyRefundRequest: '',
      content: content,
    }).save();

    const newHire = await Report.findOne({ hireId: hireId });

    report = await report
      .populate({
        path: 'hireId',
      })
      .populate({
        path: 'userId',
        select: 'id username',
      });

    pubSub.publish(REPORT_CREATED, { reportCreated: report });

    let hire = await Hire.findOneAndUpdate(
      { _id: hireId },
      { reportId: newHire.id, beReported: true }
    );

    hire = await hire
      .populate({
        path: 'clientId',
        select: 'id username image',
      })
      .populate({
        path: 'hiredPersonId',
        select: 'id username image',
      })
      .populate({
        path: 'feedbackId',
        select: 'id content star',
      })
      .populate({
        path: 'reportId',
        select: 'id refundRequest responded',
      })
      .execPopulate();

    pubSub.publish(REFUND_REQUEST_TO_LESSOR_SUBSCRIPTION, { sendRefundRequestToLessorSubscription: hire }
    );

    return report;
  },

};

const Subscription = {
  reportCreated: {
    subscribe: () => pubSub.asyncIterator('REPORT_CREATED')
  },
};

export default { Query, Mutation, Subscription };
