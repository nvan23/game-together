import mongoose from 'mongoose';
import uuid from 'uuid/dist/v4';
import { pubSub } from '../utils/apollo-server';

import {
  FEEDBACK_CREATED,
} from '../constants/Subscriptions';

const Query = {
  /**
   *  Gets all feedback from player
   * @param {string} userId user id
   */
  getFeedbacks: async (root, { userId }, { Feedback }) => {
    const feedbacks = await Feedback.find({ playerId: userId });
    return feedbacks;
  },

};

const Mutation = {
  /**
   * Creates a feedback
   *
   * @param {string} userId user id
   * @param {string} clientId client id
   * @param {string} content client give content of the feedback  
   * @param {string} hours rental hours
   * @param {string} star customers evaluate the quality of rental services by the number of stars
   */
  createFeedback: async (root, { input: { hireId, content, hours, star } }, { Feedback, Hire }) => {
    const uuidTemp = uuid();

    const feedback = await new Feedback({
      uuid: uuidTemp,
      content: content,
      hours: parseInt(hours),
      star: star,
    }).save();

    const newFeedback = await Feedback.findOne({ uuid: uuidTemp });

    console.log(newFeedback)

    let hire = await Hire.findOneAndUpdate({ _id: hireId }, { feedbackStatus: true, feedbackId: mongoose.Types.ObjectId(newFeedback.id) })
      .populate({
        path: 'clientId',
        select: 'id username image',
      })
      .populate({
        path: 'hiredPersonId',
        select: 'id username image',
      })
      .populate({
        path: 'reportId',
        select: 'id refundRequest responded relyRefundRequest',
      })
      .populate({
        path: 'feedbackId',
        select: 'id content star',
      });

    pubSub.publish(FEEDBACK_CREATED, { feedbackCreated: hire });

    return feedback;
  },

  /**
   * Delete a feedback
   *
   * @param {string} id feedback id
   */
  deleteFeedback: async (root, { input: { id } }, { Feedback }) => {
    const feedback = await Feedback.findByIdAndRemove(id);
    return feedback;
  },

  /**
   * Modify a feedback
   *
   * @param {string} id feedback id
   * @param {string} content the content of the feedback to modify
   * @param {number} star feedback id
   */
  editFeedback: async (root, { input: { id, content, star } }, { Feedback }) => {
    const feedback = await Feedback.findOneAndUpdate({ _id: id }, { content: content, star: star });
    return feedback;
  },
};

const Subscription = {
  /**
   * Subscribes to my hire created event
   */
  feedbackCreated: {
    subscribe: () => pubSub.asyncIterator('FEEDBACK_CREATED')
  },
};

export default { Query, Mutation, Subscription };
