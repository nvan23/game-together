/**
 * Convert hours to time value
 *
 * @param {number} hours
 */
export const computeCost = (hours) => {
  return parseInt(price) * parseInt(hours);
};

export var timeoutContainer = {};
export var setTimeoutContainer = {};
export var setIntervalContainer = {};

export const getTimeoutContainer = () => {
  return setTimeoutContainer;
}

export const addHireToTimeoutContainer = (key, value) => {
  timeoutContainer[key] = value;
  return timeoutContainer;
}

export const addHireIntoSetTimeoutContainer = (key, value) => {
  setTimeoutContainer[key] = value;
  return setTimeoutContainer;
}

export const addHireIntoSetIntervalContainer = (key, value) => {
  setIntervalContainer[key] = value;
  return setIntervalContainer;
}

export const clearTimeoutHireFromSetTimeoutContainer = (key) => {
  setTimeoutContainer[key] = 'closed';
  return setTimeoutContainer;
}

export const clearTimeoutHireFromSetIntervalContainer = (key) => {
  setIntervalContainer[key] = 'closed';
  return setIntervalContainer;
}

export const clearTimeoutContainer = () => {
  timeoutContainer = {}
  return timeoutContainer;
}