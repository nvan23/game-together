/**
 * Compute cost
 *
 * @param {number} price
 * @param {number} hours
 */
export const computeCost = (price, hours) => {
  return parseInt(price) * parseInt(hours);
};

/**
 * Compute balance for client
 *
 * @param {number} price
 * @param {number} hours
 * @param {number} oldBalance
 */
export const computeClientBalance = (oldBalance, price, hours) => {
  return parseInt(oldBalance) - parseInt(price) * parseInt(hours);
};

/**
 * Compute balance for player
 *
 * @param {number} price
 * @param {number} hours
 * @param {number} oldBalance
 */
export const computePlayerBalance = (oldBalance, price, hours) => {
  return parseInt(oldBalance) + parseInt(price) * parseInt(hours);
};


