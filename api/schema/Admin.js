import { gql } from 'apollo-server-express';

/**
 * Admin schema
 */
const AdminSchema = gql`
  # ---------------------------------------------------------
  # Model Objects
  # ---------------------------------------------------------
  type Admin {
    id: ID!
    fullName: String!
    gender: String
    country: String
    description: String
    email: String!
    username: String!
    password: String!
    resetToken: String
    resetTokenExpiry: String
    image: File
    imagePublicId: String
    coverImage: File
    coverImagePublicId: String
    isOnline: Boolean
    likes: [Like]
    comments: [Comment]
    followers: [Follow]
    following: [Follow]
    notifications: [NotificationPayload]
    createdAt: String
    updatedAt: String
  }

  type AdminFile {
    filename: String!
    mimetype: String!
    encoding: String!
  }

  type AdminToken {
    token: String!
  }

  type AdminSuccessMessage {
    message: String!
  }

  # ---------------------------------------------------------
  # Input Objects
  # ---------------------------------------------------------
  input AdminSignInInput {
    emailOrUsername: String!
    password: String
  }

  input AdminSignUpInput {
    email: String!
    username: String!
    fullName: String!
    password: String!
  }

  input AdminRequestPasswordResetInput {
    email: String!
  }

  input AdminResetPasswordInput {
    email: String!
    token: String!
    password: String!
  }

  input AdminUploadUserPhotoInput {
    id: ID!
    image: Upload!
    imagePublicId: String
    isCover: Boolean
  }

  # ---------------------------------------------------------
  # Return Payloads
  # ---------------------------------------------------------
  type AdminPayload {
    id: ID!
    fullName: String
    gender: String
    country: String
    description: String
    email: String
    username: String
    password: String
    image: String
    imagePublicId: String
    coverImage: String
    coverImagePublicId: String
    isOnline: Boolean
    createdAt: String
    updatedAt: String
  }

  type AdminsPayload {
    users: [AdminPayload]!
    count: String!
  }

  type AdminIsUserOnlinePayload {
    userId: ID!
    isOnline: Boolean
  }

  # ---------------------------------------------------------
  # Queries
  # ---------------------------------------------------------
  extend type Query {
    # Verifies reset password token
    verifyResetPasswordTokenAdmin(email: String, token: String!): AdminSuccessMessage

    # Gets the currently logged in user
    getAuthAdmin: AdminPayload

    # Gets user by username or by id
    getAdmin(username: String, id: ID): AdminPayload
  }

  # ---------------------------------------------------------
  # Mutations
  # ---------------------------------------------------------
  extend type Mutation {
    # Signs in user
    signinAdmin(input: AdminSignInInput!): AdminToken

    # Signs up user
    signupAdmin(input: AdminSignUpInput!): AdminToken

    # Requests reset password
    requestPasswordResetAdmin(input: AdminRequestPasswordResetInput!): AdminSuccessMessage

    # Resets user password
    resetPasswordAdmin(input: AdminResetPasswordInput!): AdminToken

    # Uploads user Profile or Cover photo
    uploadUserPhotoAdmin(input: AdminUploadUserPhotoInput!): AdminPayload
  }

  # ---------------------------------------------------------
  # Subscriptions
  # ---------------------------------------------------------
  extend type Subscription {
    # Subscribes to is user online event
    isUserOnlineAdmin(authUserId: ID!, userId: ID!): AdminIsUserOnlinePayload
  }
`;

export default AdminSchema;
