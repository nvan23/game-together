import { gql } from 'apollo-server-express';

/**
 * TimeoutContainer schema
 */
const TimeoutContainerSchema = gql`
  # ---------------------------------------------------------
  # Model Objects
  # ---------------------------------------------------------

  type TimeoutContainer {
    id: ID!
    key: String!
    value: String!
    createdAt: String
    updatedAt: String
  }

  type TimeoutContainerPayLoad {
    count: Int!
    timeoutContainers: [TimeoutContainer]!
  }

  type BackupTimeoutContainerPayLoad {
    count: Int!
  }

  # ---------------------------------------------------------
  # Queries
  # ---------------------------------------------------------
  extend type Query {
    # Gets my all hires
    getAllTimeoutContainers: TimeoutContainerPayLoad

    # Gets a hire
    getTimeoutContainer(key: String!): TimeoutContainer
  }

  # ---------------------------------------------------------
  # Mutations
  # ---------------------------------------------------------
  extend type Mutation {
    # Create a hire relationship between users
    backup: TimeoutContainerPayLoad
  }
`;

export default TimeoutContainerSchema;
