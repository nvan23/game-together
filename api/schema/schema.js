import { gql } from 'apollo-server-express';

import UserSchema from './User';
import AdminSchema from './Admin';
import PostSchema from './Post';
import MessageSchema from './Message';
import LikeSchema from './Like';
import FollowSchema from './Follow';
import HireSchema from './Hire';
import TimeoutContainerSchema from './TimeoutContainer';
import GameSchema from './Game';
import DonateSchema from './Donate';
import FeedbackSchema from './Feedback';
import ReportSchema from './Report';
import CommentSchema from './Comment';
import NotificationSchema from './Notification';

const schema = gql`
  type Query {
    _empty: String
  }

  type Mutation {
    _empty: String
  }

  type Subscription {
    _empty: String
  }

  ${UserSchema}
  ${AdminSchema}
  ${PostSchema}
  ${MessageSchema}
  ${FollowSchema}
  ${HireSchema}
  ${TimeoutContainerSchema}
  ${GameSchema}
  ${DonateSchema}
  ${FeedbackSchema}
  ${ReportSchema}
  ${LikeSchema}
  ${CommentSchema}
  ${NotificationSchema}
`;

export default schema;
