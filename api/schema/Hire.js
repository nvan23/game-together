import { gql } from 'apollo-server-express';

/**
 * Hire schema
 */
const HireSchema = gql`
  # ---------------------------------------------------------
  # Model Objects
  # ---------------------------------------------------------

  # enum HireStage {
  #   wait
  #   ongoing
  #   close
  # }

  # enum HireStatus {
  #   ongoing
  #   success
  #   fail
  # }

  type Hire {
    id: ID!
    clientId: UserPayload!
    hiredPersonId: UserPayload!
    feedbackId: Feedback
    reportId: Report
    feedbackStatus: Boolean
    timeout: Int
    price: Int!
    hours: Int!
    cost: Int!
    message: String
    messageToThank: String
    readyClientStatus: Boolean!
    stage: String!
    status: String!
    accomplished: Boolean!
    beReported: Boolean!
    refundRequestStatus: Boolean
    createdAt: String
    updatedAt: String
  }

  type HirePayLoad {
    id: ID!
    clientId: UserPayload!
    hiredPersonId: UserPayload!
    feedbackId: Feedback
    reportId: Report
    feedbackStatus: Boolean
    price: Int!
    hours: Int!
    timeout: Int
    cost: Int!
    message: String
    messageToThank: String
    readyClientStatus: Boolean!
    stage: String!
    status: String!
    accomplished: Boolean!
    beReported: Boolean!
    refundRequestStatus: Boolean
    createdAt: String
    updatedAt: String
  }

  type HireTimeoutPayLoad {
    timeout: Int
    hire: HirePayLoad
  }

  type HiresPayload {
    count: Int!
    hires: [Hire]!
  }

  type TimeoutContainerChangeStatusPayLoad {
    key: String!
    value: String!
  }

  type StatusHirePayLoad {
    timeout: TimeoutContainerChangeStatusPayLoad
    hire: Hire
  }


  # ---------------------------------------------------------
  # Input Objects
  # ---------------------------------------------------------
  input GetHireInput {
    userId: String!
    hireId: String!
  }

  input CreateHireInput {
    clientId: ID!
    hiredPersonId: ID!
    hours: Int!
    message: String
  }

  input CloseHireInput {
    id: ID!
    starValue: Int!
    feedback: String!
  }

  input ReportHireInput {
    id: ID!
    comment: String!
  }

  input ChangeHireStateInput {
    hireId: String!
    stage: String!
  }
  
  input ClearTimeoutHireInput {
    hireId: ID!
  }

  input ChangeReadyClientStatusInput {
    hireId: ID!
  } 

  input ReplyRefundRequestInput {
    clientId: ID!
    hiredPersonId: ID!
    hireId: ID!
    reportId: ID!
    value: Boolean
    hours: Int
  } 

  # ---------------------------------------------------------
  # Queries
  # ---------------------------------------------------------
  extend type Query {
    # Gets my all hires
    getAllHires: HiresPayload

    # Gets all hires of client
    getAllHiresToWork: [Hire]

    # Gets a hire
    getHire(hireId: String!): HireTimeoutPayLoad
  }

  # ---------------------------------------------------------
  # Mutations
  # ---------------------------------------------------------
  extend type Mutation {
    # Create a hire relationship between users
    createHire(input: CreateHireInput!): Hire

    # Create a hire relationship between users
    changeHireState(input: ChangeHireStateInput): Hire

    clearTimeoutHire(input: ClearTimeoutHireInput): String

    # CLose a hire relationship between users
    closeHire(input: CloseHireInput!): Hire

    # Report a hire relationship between users
    reportHire(input: ReportHireInput!): Hire

    # Change status of client into ready
    changeReadyClientStatus(input: ChangeReadyClientStatusInput): Hire!

    replyRefundRequest(input: ReplyRefundRequestInput): Hire
  
    backupAllTimeoutHires: HiresPayload
  }

  # ---------------------------------------------------------
  # Subscriptions
  # ---------------------------------------------------------
  extend type Subscription {
    # Subscribes to my hire created event
    hireCreated(authUserId: ID!): Hire

    changeReadyClientStatusSubscription: Hire

    sendRefundRequestToLessorSubscription: Hire

    stageChangedSubscription: Hire

    overTimeoutSubscription: Hire
  }
`;

export default HireSchema;
