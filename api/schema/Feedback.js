import { gql } from 'apollo-server-express';

/**
 * Feedback schema
 */
const FeedbackSchema = gql`
  # ---------------------------------------------------------
  # Model Objects
  # ---------------------------------------------------------
  type Feedback {
    id: ID
    content: String
    hours: Int
    star: Int
    createdAt: String
    updatedAt: String
  }

  # ---------------------------------------------------------
  # Input Objects
  # ---------------------------------------------------------
  input CreateFeedbackInput {
    hireId: ID
    content: String
    hours: Int
    star: Int
  }

  input EditFeedbackInput {
    id: ID
    content: String
    star: Int
  }

  input RemoveFeedbackInput {
    id: ID!
  }

  # ---------------------------------------------------------
  # Queries
  # ---------------------------------------------------------
  extend type Query {
    # Gets all feedback from player
    getFeedbacks(userId: String!): [Feedback]
  }

  # ---------------------------------------------------------
  # Mutations
  # ---------------------------------------------------------
  extend type Mutation {
    # Create a feedback between users
    createFeedback(input: CreateFeedbackInput): Feedback

    # Delete a feedback between users
    deleteFeedback(input: EditFeedbackInput): Feedback

    # Modify a feedback between users
    editFeedback(input: RemoveFeedbackInput): Feedback
  }

  # ---------------------------------------------------------
  # Subscriptions
  # ---------------------------------------------------------
  extend type Subscription {
    feedbackCreated: Hire
  }

`;

export default FeedbackSchema;
