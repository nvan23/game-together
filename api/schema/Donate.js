import { gql } from 'apollo-server-express';

/**
 * Donate schema
 */
const DonateSchema = gql`
  # ---------------------------------------------------------
  # Model Objects
  # ---------------------------------------------------------
  type Donate {
    id: ID!
    user: ID!
    donatedPerson: ID!
    amountOfMoney: Int!
    message: String
    createdAt: String
    updatedAt: String
  }

  # ---------------------------------------------------------
  # Input Objects
  # ---------------------------------------------------------
  input CreateDonateInput {
    userId: ID!
    donatedPerson: ID!
    amountOfMoney: Int!
    message: String
  }

  # ---------------------------------------------------------
  # Queries
  # ---------------------------------------------------------
  extend type Query {
    # Gets all donates
    getAllDonates: [Donate]

    # Gets all donates of player
    getDonates: [Donate]

    # Gets donate by id
    getDonate(id: String!): Donate
  }

  # ---------------------------------------------------------
  # Mutations
  # ---------------------------------------------------------
  extend type Mutation {
    # Create a donate between users
    createDonate(input: CreateDonateInput!): Donate
  }
`;

export default DonateSchema;
