import { gql } from 'apollo-server-express';

/**
 * Report schema
 */
const ReportSchema = gql`
  # ---------------------------------------------------------
  # Model Objects
  # ---------------------------------------------------------
  type Report {
    id: ID
    hireId: Hire
    clientId: UserPayload
    responded: Boolean
    refundRequest: Boolean
    relyRefundRequest: String
    content: String
    createdAt: String
    updatedAt: String
  }

  # ---------------------------------------------------------
  # Input Objects
  # ---------------------------------------------------------
  input CreateReportClientInput {
    hireId: ID!
    clientId: ID!
    content: String
    refundRequest: Boolean
  }

  # ---------------------------------------------------------
  # Queries
  # ---------------------------------------------------------
  extend type Query {
    # Gets all Report from player
    getAllReports: [Report]
  }

  # ---------------------------------------------------------
  # Mutations
  # ---------------------------------------------------------
  extend type Mutation {
    # Create a Report between users
    createReportClient(input: CreateReportClientInput!): Report
  }

    # ---------------------------------------------------------
  # Subscriptions
  # ---------------------------------------------------------
  extend type Subscription {
    # Subscribes to my hire created event
    reportCreated: Report
  }
`;

export default ReportSchema;
