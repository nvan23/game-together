import { gql } from 'apollo-server-express';

/**
 * Game schema
 */
const GameSchema = gql`
  # ---------------------------------------------------------
  # Model Objects
  # ---------------------------------------------------------
  type Game {
    id: ID!
    name: String!
    createdAt: String
    updatedAt: String
  }

  # ---------------------------------------------------------
  # Input Objects
  # ---------------------------------------------------------
  input CreateGameInput {
    name: String!
  }

  input DeleteGameInput {
    id: ID!
  }

  input RenameGameInput {
    id: ID!
    name: String!
  }

  # ---------------------------------------------------------
  # Queries
  # ---------------------------------------------------------
  extend type Query {
    # Gets all games
    getAllGames: [Game]

    # Gets game by id
    getGame(id: String!): Game
  }

  # ---------------------------------------------------------
  # Mutations
  # ---------------------------------------------------------
  extend type Mutation {
    # Create a game
    createGame(input: CreateGameInput!): Game

    # Delete a game
    deleteGame(input: DeleteGameInput!): Game

    # Rename a game
    renameGame(input: RenameGameInput!): Game
  }
`;

export default GameSchema;
