# Game Together

Game Together is a project developed from [Create Social Network](https://github.com/DimiMikadze/create-social-network). It inherited all of the features from the CSN and added new features to fit subject area. The main content developed is to manage the service of hiring other people's game time to play with him.

## Screenshots of the app

|                                        Store                                        |                                        Hires                                        |                                        Workplace                                        |
| :--------------------------------------------------------------------------------: | :------------------------------------------------------------------------------------: | :-----------------------------------------------------------------------------------: |
| ![](https://res.cloudinary.com/dtcjvrp7p/image/upload/v1611236480/03d54488e553150d4c42_evqlta.jpg) | ![](https://res.cloudinary.com/dtcjvrp7p/image/upload/v1611236481/e54f19d4b60f46511f1e_ptad3x.jpg) | ![](https://res.cloudinary.com/dtcjvrp7p/image/upload/v1611236480/cc8e380996d2668c3fc3_xikyxn.jpg) |

## Installation

```sh
cd game-together/frontend
npm install

cd game-together/api
npm install
```


## Requirements and Configuration

You’ll need to have Node 8.16.0 or Node 10.16.0 or later version on your local development machine

By default, the app uses MongoDB hosted on [mLab](https://mlab.com/) and [Cloudinary](https://cloudinary.com/) CDN for hosting images. We have created a demo user for mLab and Cloudinary so you can run the app locally without adding Mongo URL and Cloudinary API Key, however when you start developing your application it is recommended to replace that information with your own, so that everyone has their own Database and CDN.

### Replacing Mongo URL

Replace `MONGO_URL` value in `api/.env` file with your `mLab` database url or with local one.

### Replacing Cloudinary API Key

Grab `Cloud name` `API Key` and `API Secret` from Cloudinary dashboard and replace corresponding values inside `api/.env` file.

### Mail Provider

For password reset functionality you will need to replace Email Provider values also in `api/.env` file.

Once the installation is done, you can open your project folder:

```sh
cd game-together
```

And start the application with `npm start` or `yarn start` that will run the app in development mode.
Open [http://localhost:3000/](http://localhost:3000/) to view it in the browser.

The page will automatically reload if you make changes to the code.
