var fs = require('fs');

export const getHiredIdToFocusOnTable = () => {
  const hiresIdToExpandRowData = localStorage.getItem('hiresIdToExpandRow');
  let hiresIdToExpandRow = [];
  hiresIdToExpandRow = JSON.parse(hiresIdToExpandRowData);
  return hiresIdToExpandRow;
}

export const addHiredIdToFocusOnTable = (hireId) => {
  localStorage.removeItem('hiresIdToExpandRow');
  let hiresIdToExpandRow = [];
  hiresIdToExpandRow.push(hireId);
  localStorage.setItem('hiresIdToExpandRow', JSON.stringify(hiresIdToExpandRow));
  return hiresIdToExpandRow;
}

export var targetTime = 0;

export const setTargetTime = (hours, timeout) => {
  let targetTime = new Date().getTime();
  targetTime = targetTime + parseInt(hours) * 3600000 - parseInt(timeout);
  return targetTime;
}