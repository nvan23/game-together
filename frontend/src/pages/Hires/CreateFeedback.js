import React, { useState } from 'react';
import { useApolloClient } from '@apollo/client';

import {
  Button,
  Form,
  Rate,
  Input,
} from 'antd';

import {
  CREATE_FEEDBACK
} from 'graphql/feedback';

import {
  GET_ALL_HIRES,
} from 'graphql/hire';

const CreateFeedback = ({ hireId, clientId, playerId, hours }) => {
  const client = useApolloClient();
  const [{ feedbackContent }, setFeedbackContent] = useState({ feedbackContent: '' });
  const [starRate, setStarRate] = useState(5);

  const [form] = Form.useForm();
  const { TextArea } = Input;

  const formItemLayout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 14,
    },
  };

  const onChangeFeedbackContent = ({ target: { value } }) => {
    setFeedbackContent({ feedbackContent: value });
  }

  const onChangeStarRate = (value) => {
    setStarRate(value);
  }

  const handleSubmitFeedback = async () => {
    try {
      await client.mutate({
        mutation: CREATE_FEEDBACK,
        variables: {
          input: {
            hireId: hireId,
            clientId: clientId,
            playerId: playerId,
            hours: parseInt(hours),
            content: feedbackContent,
            star: parseInt(starRate),
          }
        },
        refetchQueries: () => [
          {
            query: GET_ALL_HIRES,
          },
        ],
      });
    } catch (err) { }
  }

  return (

    <Form
      {...formItemLayout}
      form={form}
      onFinish={handleSubmitFeedback}
    >
      <Form.Item name="rate" label="Rate">
        <Rate
          value={starRate}
          onChange={onChangeStarRate}
        />
      </Form.Item>

      <Form.Item name="feedback" label="Feedback">
        <TextArea
          value={feedbackContent}
          onChange={onChangeFeedbackContent}
          rows={4}
        />
      </Form.Item>
      <Form.Item
        wrapperCol={{
          span: 12,
          offset: 6
        }}
      >
        <Button
          type="primary"
          htmlType="submit"
        >
          Submit
        </Button>
      </Form.Item>
    </Form>
  )
};

export default CreateFeedback;
