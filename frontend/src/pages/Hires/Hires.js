import React, { useEffect, useState, Fragment } from 'react';
import { Link, generatePath } from 'react-router-dom';
import styled from 'styled-components';
import { useApolloClient } from '@apollo/client';

import { Container } from 'components/Layout';
import Skeleton from 'components/Skeleton';
import Empty from 'components/Empty';
import Head from 'components/Head';

import { timeAgo } from 'utils/date';
import { getHiredIdToFocusOnTable, setTargetTime } from 'utils/cache';

import {
  Table,
  Typography,
  Avatar,
  Image,
  Steps,
  Divider,
  Button,
  Tag,
  Descriptions,
  Form,
  Rate,
  Input,
  Spin,
  notification,
  Modal,
  Checkbox,
} from 'antd';

import {
  DownOutlined,
  UpOutlined,
  CheckCircleOutlined,
  SyncOutlined,
  CloseCircleOutlined,
  ClockCircleOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";

import CountDown from 'ant-design-pro/lib/CountDown';

import {
  CREATE_FEEDBACK
} from 'graphql/feedback';

import {
  GET_ALL_HIRES,
  GET_HIRE,
  REPORT_HIRE,
  CREATE_REPORT,
  GET_ALL_HIRES_TO_WORK_SUBSCRIPTION,
  CHANGE_READY_CLIENT_STATUS_SUBSCRIPTION,
  REFUND_REQUEST_TO_LESSOR_SUBSCRIPTION,
  GET_ALL_TIME_OUT_CONTAINER,
  CHANGE_HIRE_STAGE,
  CLEAR_TIMEOUT_HIRE,
  OVER_TIME_OUT_SUBSCRIPTION,
  FEEDBACK_CREATED,
} from 'graphql/hire';

import { PEOPLE_PAGE_USERS_LIMIT } from 'constants/DataLimit';
import { WAIT, ONGOING, CLOSE } from 'constants/Hire';

import { useQuery } from '@apollo/client';

import { useStore } from 'store';

import * as Routes from 'routes';

import { A } from 'components/Text';

const Root = styled(Container)`
  margin-top: ${(p) => p.theme.spacing.lg};

  @media (min-width: ${(p) => p.theme.screen.lg}) {
    margin-left: ${(p) => p.theme.spacing.lg};
    padding: 0;
  }
`;

const ButtonContainer = styled.div`
  margin-left: 20px;
`

const PeopleContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(180px, 3fr));
  grid-auto-rows: auto;
  grid-gap: 20px;
  margin-bottom: ${(p) => p.theme.spacing.lg};
`;

const Message = styled(Link)`
  text-decoration: none;
  font-size: ${(p) => p.theme.font.size.xs};
`;

const Name = styled.span`
  font-size: ${(p) => p.theme.font.size.xs};
  font-weight: ${(p) => p.theme.font.weight.bold};
  color: ${(p) => p.theme.colors.primary.main};
`;

const ExpandableContainer = styled.div`
  padding: 20px 40px;
`
const StepContainer = styled.div`
  padding: 0 0 10px 30px;
`
const ButtonStepContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin: 20px 0 0 30px;
`

const FeedbackContainer = styled.div`
  display: block;
`

/**
 * Hires management page
 */
const Hires = () => {
  const [{ auth }] = useStore();
  const client = useApolloClient();
  const [current] = useState(1);
  const [isDisabledStartHireButton, setDisabledStartHireButton] = useState(false);
  const [isDisabledCancelHireButton, setDisabledCancelHireButton] = useState(true);
  const [isDisabledReportHireButton, setDisabledReportHireButton] = useState(false);
  const [isDisabledCloseHireButton, setDisabledCloseHireButton] = useState(true);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [refundStatusRequest, setRefundStatusRequest] = useState(false);
  const [{ feedbackContent }, setFeedbackContent] = useState({ feedbackContent: '' });
  const [{ reportContent }, setReportContent] = useState({ reportContent: '' });
  const [starRate, setStarRate] = useState(5);

  const { subscribeToMoreTimeout, data: Timeout } = useQuery(GET_ALL_TIME_OUT_CONTAINER, {
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
  });

  // const { } = useQuery(GET_HIRE, {
  //   variables: {}
  // })

  const { subscribeToMore, data: Hires, loading, networkStatus } = useQuery(GET_ALL_HIRES, {
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
  });

  useEffect(() => {
    const unsubscribe = subscribeToMore({
      document: GET_ALL_HIRES_TO_WORK_SUBSCRIPTION,
      variables: { authUserId: auth.user.id },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const newHire = subscriptionData.data.hireCreated;
        const mergedHires = [...prev.getAllHires.hires, newHire];

        return { getAllHires: { hires: mergedHires } };
      },
    });

    return () => {
      unsubscribe();
    };
  }, [auth.user.id, subscribeToMore]);

  useEffect(() => {
    const unsubscribe = subscribeToMore({
      document: CHANGE_READY_CLIENT_STATUS_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const hireChanged = subscriptionData.data.changeReadyClientStatusSubscription;
        const mergedHires = [...prev.getAllHires.hires, hireChanged];

        return { getAllHires: { hires: mergedHires } };
      },
    });

    return () => {
      unsubscribe();
    };
  }, [subscribeToMore]);

  useEffect(() => {
    const unsubscribe = subscribeToMore({
      document: OVER_TIME_OUT_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const hireChanged = subscriptionData.data.overTimeoutSubscription;
        const mergedHires = [...prev.getAllHires.hires, hireChanged];

        return { getAllHires: { hires: mergedHires } };
      },
    });

    return () => {
      unsubscribe();
    };
  }, [subscribeToMore]);

  useEffect(() => {
    const unsubscribe = subscribeToMore({
      document: FEEDBACK_CREATED,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const hireChanged = subscriptionData.data.feedbackCreated;
        const mergedHires = [...prev.getAllHires.hires, hireChanged];

        return { getAllHires: { hires: mergedHires } };
      },
    });

    return () => {
      unsubscribe();
    };
  }, [subscribeToMore]);

  useEffect(() => {
    const unsubscribe = subscribeToMore({
      document: REFUND_REQUEST_TO_LESSOR_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const hireChanged = subscriptionData.data.sendRefundRequestToLessorSubscription;
        const mergedHires = [...prev.getAllHires.hires, hireChanged];

        return { getAllHires: { hires: mergedHires } };
      },
    });

    return () => {
      unsubscribe();
    };
  }, [subscribeToMore]);

  const { Text, Title } = Typography;
  const [form] = Form.useForm();
  const { TextArea } = Input;

  const { Step } = Steps;

  const renderContent = () => {
    if (loading && networkStatus === 1) {
      return (
        <PeopleContainer>
          <Skeleton height={280} count={PEOPLE_PAGE_USERS_LIMIT} />
        </PeopleContainer>
      );
    }

    // const { timeoutHires } = Timeout.getAllTimeoutContainers;

    const { hires } = Hires.getAllHires;
    let countIndex = 1;
    if (hires.length < 1) return <Empty text="No hire yet." />;

    const dataSourceHireTable = hires.map((hire) => {
      const unitHire = {
        index: countIndex++,
        key: hire.id,
        name: hire.id,
        playerId: hire.hiredPersonId.id,
        clientId: hire.clientId.id,
        avatarPlayer: hire.hiredPersonId.image,
        usernamePlayer: hire.hiredPersonId.username,
        feedback: hire.feedbackId ? hire.feedbackId.content : '',
        starRate: hire.feedbackId ? hire.feedbackId.star : 0,
        feedbackStatus: hire.feedbackStatus,
        price: hire.price,
        hours: hire.hours,
        timeout: hire.timeout,
        cost: hire.cost,
        message: hire.message,
        messageToThank: hire.messageToThank,
        readyClientStatus: hire.readyClientStatus,
        responded: hire.reportId === null ? false : hire.reportId.responded,
        relyRefundRequest: hire.recordId === undefined ? '' : hire.recordId.relyRefundRequest,
        stage: hire.stage,
        status: hire.status,
        createdAt: hire.createdAt,
      }
      return unitHire;
    })

    let uniqueUsernamePlayer = [...new Set(hires.map((hire) => hire.hiredPersonId.username))];
    const usernamePlayerToFilter = uniqueUsernamePlayer.map((usernamePlayer) => {
      const user = {
        text: usernamePlayer,
        value: usernamePlayer,
      }
      return user;
    });

    const stageToFilter = [
      {
        text: 'Wait',
        value: 'wait',
      },
      {
        text: 'Ongoing',
        value: 'ongoing',
      },
      {
        text: 'Close',
        value: 'close',
      },
    ];

    const statusToFilter = [
      {
        text: 'Ongoing',
        value: 'ongoing',
      },
      {
        text: <Text type="success">Success</Text>,
        value: 'success',
      },
      {
        text: <Text type="danger">Fail</Text>,
        value: 'fail',
      },
    ];

    const columns = [
      {
        title: 'Index',
        dataIndex: 'index',
        key: 'index',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.index - b.index,
      },
      {
        title: 'Avatar',
        dataIndex: 'avatarPlayer',
        key: 'avatarPlayer',
        align: 'center',
        render: avatarPlayer => (
          <Avatar
            src={<Image src={avatarPlayer} />}
            size={40}
          />
        )
      },
      {
        title: 'Player',
        dataIndex: 'usernamePlayer',
        key: 'usernamePlayer',
        align: 'center',
        filters: usernamePlayerToFilter,
        onFilter: (value, record) => record.usernamePlayer.indexOf(value) === 0,
        render: usernamePlayer => (
          <A to={generatePath(Routes.USER_PROFILE, { username: usernamePlayer })}>
            <Name>{usernamePlayer}</Name>
          </A>
        )
      },
      {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.price - b.price,
        render: price => (`$${price}`)
      },
      {
        title: 'Hours',
        dataIndex: 'hours',
        key: 'hours',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.hours - b.hours,
      },
      {
        title: 'Cost',
        dataIndex: 'cost',
        key: 'cost',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.cost - b.cost,
        render: cost => (`$${cost}`)
      },
      {
        title: 'Stage',
        dataIndex: 'stage',
        key: 'stage',
        align: 'center',
        filters: stageToFilter,
        onFilter: (value, record) => record.stage.indexOf(value) === 0,
        render: stage => (
          stage === 'close'
            ? (<Tag icon={<ExclamationCircleOutlined />} color="default">Close</Tag>)
            : (stage === 'wait'
              ? (<Tag icon={<ClockCircleOutlined />} color="default">Wait</Tag>)
              : (<Tag icon={<SyncOutlined />} color="processing">Ongoing</Tag>))
        )
      },
      {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
        align: 'center',
        filters: statusToFilter,
        onFilter: (value, record) => record.status.indexOf(value) === 0,
        render: status => (
          status === 'success'
            ? (<Tag icon={<CheckCircleOutlined />} color="success">Success</Tag>)
            : (status === 'fail'
              ? (<Tag icon={<CloseCircleOutlined />} color="error">Fail</Tag>)
              : (<Tag icon={<SyncOutlined />} color="processing">Ongoing</Tag>))
        )
      },
      {
        title: 'Created At',
        dataIndex: 'createdAt',
        key: 'createdAt',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => parseInt(a.createdAt) - parseInt(b.createdAt),
        render: createdAt => (
          <>
            {/* for Vietnam, set value in toLocalDateString is "vi-VN": "dd/MM/yyyy" */}
            {`${timeAgo(createdAt)} ago (${new Date(parseInt(createdAt)).toLocaleDateString("en-US")})`}
          </>
        )
      },
    ];

    const handleCurrentStep = (stage) => {
      switch (stage) {
        case WAIT:
          return 1
        case ONGOING:
          setDisabledStartHireButton(true);
          setDisabledCancelHireButton(false);
          setDisabledCloseHireButton(false);
          return 2
        case CLOSE:
          setDisabledStartHireButton(true);
          setDisabledCloseHireButton(true);
          setDisabledCancelHireButton(true);
          return 4
        default:
          return 0
      }
    }

    let totalHours = 0;
    let totalCost = 0;

    dataSourceHireTable.forEach(({ hours, cost }) => {
      totalHours += hours;
      totalCost += cost
    });

    const formItemLayout = {
      labelCol: {
        span: 6,
      },
      wrapperCol: {
        span: 14,
      },
    };

    const onChangeFeedbackContent = ({ target: { value } }) => {
      setFeedbackContent({ feedbackContent: value });
    }

    const onChangeReportContent = ({ target: { value } }) => {
      setReportContent({ reportContent: value });
    }

    const onChangeStarRate = (value) => {
      setStarRate(value);
    }

    const getHiredIdToFocusOnTableClient = () => {
      const hiresIdToExpandRowData = localStorage.getItem('hiresIdToExpandRow');
      let hiresIdToExpandRow = [];
      hiresIdToExpandRow = JSON.parse(hiresIdToExpandRowData);
      return hiresIdToExpandRow;
    }

    const layout = {
      labelCol: {
        span: 6,
      },
      wrapperCol: {
        span: 16,
      },
    };

    const tailLayout = {
      wrapperCol: {
        offset: 6,
        span: 16,
      },
    };

    const showModal = () => {
      setIsModalVisible(true);
    };

    const handleCancel = () => {
      setIsModalVisible(false);
    };

    const onChangeCheckBox = (e) => {
      setRefundStatusRequest(e.target.checked);
    };

    return (
      <Fragment>
        <Table
          bordered
          size="middle"
          rowKey="key"
          loading={dataSourceHireTable.length > 0 ? false : true}
          columns={columns}
          expandable={{
            expandedRowRender: record => (
              <ExpandableContainer>
                <StepContainer>
                  <Steps current={handleCurrentStep(record.stage)} percent={0} size='small'>
                    <Step
                      title="Ordered"
                      description={
                        <>
                          Find and order hire services on
                          <Message to={generatePath(Routes.STORES)}> Stores.</Message>
                        </>
                      }
                    />
                    <Step title="Wait" description="Wait for players to get ready" />
                    <Step title="Ongoing" description={
                      <>
                        {
                          record.stage === ONGOING
                            ? <CountDown style={{ fontSize: 20 }} target={setTargetTime(record.hours, record.timeout)} />
                            : ''
                        }
                      </>
                    }
                    />
                    <Step title="Close" description={"End the hiring process at TIME"} />
                  </Steps>

                  <ButtonStepContainer>
                    <Button
                      type="primary"
                      disabled={isDisabledStartHireButton || !record.readyClientStatus}
                      onClick={
                        async () => {
                          try {
                            await client.mutate({
                              mutation: CHANGE_HIRE_STAGE,
                              variables: { input: { hireId: record.key, stage: ONGOING } },
                              refetchQueries: () => [
                                { query: GET_ALL_HIRES },
                              ],
                            });
                            localStorage.removeItem('hireIdToStartProcess');
                            localStorage.setItem('hireIdToStartProcess', record.key);
                            setDisabledStartHireButton(true);
                            setDisabledCancelHireButton(false);
                          } catch (err) { }
                        }
                      }>
                      Start
                      </Button>
                    <ButtonContainer>
                      <Button
                        type="primary"
                        disabled={isDisabledCancelHireButton}
                      >
                        Cancel
                      </Button>
                    </ButtonContainer>
                    <ButtonContainer>
                      <Button
                        type="primary"
                        disabled={isDisabledCloseHireButton}
                        onClick={
                          async () => {
                            try {
                              await client.mutate({
                                mutation: CHANGE_HIRE_STAGE,
                                variables: { input: { hireId: record.key, stage: CLOSE } },
                                refetchQueries: () => [
                                  { query: GET_ALL_HIRES },
                                ],
                              });
                              await client.mutate({
                                mutation: CLEAR_TIMEOUT_HIRE,
                                variables: { input: { hireId: record.key } },
                                refetchQueries: () => [
                                  { query: GET_ALL_HIRES },
                                ],
                              });

                              setDisabledStartHireButton(true);
                              setDisabledCancelHireButton(false);
                            } catch (err) { }
                          }
                        }>
                        Close
                      </Button>
                    </ButtonContainer>
                    <ButtonContainer>
                      <Button
                        danger
                        disabled={record.responded && record.relyRefundRequest !== ''}
                        onClick={() => showModal()}
                      >
                        Report
                      </Button>
                    </ButtonContainer>
                  </ButtonStepContainer>

                </StepContainer>

                <Divider />
                <Title level={5}>{`HIRE ID: ${record.key.toUpperCase()}`}</Title>
                <p>
                  <Text strong >{` ${auth.user.username}: `}</Text>{record.message}
                </p>
                <p>
                  <Text strong >{` ${record.usernamePlayer}: `}</Text>{record.messageToThank}
                </p>
                <Message to={generatePath(Routes.MESSAGES, { userId: record.playerId })}>Chat more...</Message>
                <Divider />
                {
                  !record.feedbackStatus && record.stage === CLOSE &&
                  <FeedbackContainer>
                    <Form
                      {...formItemLayout}
                      form={form}
                      onFinish={async () => {
                        try {
                          await client.mutate({
                            mutation: CREATE_FEEDBACK,
                            variables: {
                              input: {
                                hireId: record.key,
                                hours: parseInt(record.hours),
                                content: feedbackContent,
                                star: parseInt(starRate),
                              }
                            },
                            refetchQueries: () => [
                              { query: GET_ALL_HIRES },
                            ],
                          });
                        } catch (err) { }
                      }}
                    >
                      <Form.Item name="rate" label="Rate">
                        <Rate
                          value={starRate}
                          onChange={onChangeStarRate}
                        />
                      </Form.Item>

                      <Form.Item name="feedback" label="Feedback">
                        <TextArea
                          key={record.usernamePlayer}
                          value={feedbackContent}
                          onChange={onChangeFeedbackContent}
                          rows={4}
                        />
                      </Form.Item>
                      <Form.Item
                        wrapperCol={{
                          span: 12,
                          offset: 6
                        }}
                      >
                        <Button
                          type="primary"
                          htmlType="submit"
                        >
                          Submit
                        </Button>
                      </Form.Item>
                    </Form>
                  </FeedbackContainer>
                }
                {record.feedbackStatus &&
                  <FeedbackContainer>
                    <Descriptions title="Feedback" bordered>
                      <Descriptions.Item label="To" span={3}>
                        <A to={generatePath(Routes.USER_PROFILE, { username: record.usernamePlayer })}>
                          <Name>{record.usernamePlayer}</Name>
                        </A>
                      </Descriptions.Item>
                      <Descriptions.Item label="Rate" span={3}>
                        <Rate
                          disabled={true}
                          value={record.starRate}
                        />
                      </Descriptions.Item>
                      <Descriptions.Item label="Content">
                        {record.feedback}
                      </Descriptions.Item>
                    </Descriptions>
                  </FeedbackContainer>}
                <Modal
                  title={`Report ${record.usernamePlayer}`}
                  visible={isModalVisible}
                  centered
                  width={400}
                  onCancel={handleCancel}
                  footer={null}>
                  <Form
                    {...layout}
                    form={form}
                    onFinish={
                      async () => {
                        try {
                          setIsModalVisible(false);
                          notification.info({
                            message: `Report player ${record.usernamePlayer} successfully`,
                          });
                          await client.mutate({
                            mutation: CREATE_REPORT,
                            variables: {
                              input: {
                                hireId: record.key,
                                clientId: record.clientId,
                                refundRequest: refundStatusRequest,
                                content: reportContent,
                              }
                            },
                          });
                        } catch (err) { }
                      }
                    }
                  >
                    <Form.Item label="Refund">
                      <Checkbox checked={refundStatusRequest} onChange={onChangeCheckBox}></Checkbox>
                    </Form.Item>
                    <Form.Item name="feedback" label="Content">
                      <TextArea
                        key={record.key}
                        value={reportContent}
                        onChange={onChangeReportContent}
                        rows={8}
                      />
                    </Form.Item>
                    <Form.Item {...tailLayout}>
                      <Button
                        type="primary"
                        htmlType="submit"
                      >
                        Submit
                            </Button>
                    </Form.Item>
                  </Form>
                </Modal>
              </ExpandableContainer>
            ),
            expandableRow: () => { },
            defaultExpandedRowKeys: getHiredIdToFocusOnTableClient(),
            expandIcon: ({ expanded, onExpand, record }) =>
              expanded
                ? (<UpOutlined onClick={e => onExpand(record, e)} />)
                : (<DownOutlined onClick={e => onExpand(record, e)} />),
          }}
          dataSource={dataSourceHireTable}
          summary={
            pageData => {
              let totalHoursOnPage = 0;
              let totalCostOnPage = 0;
              let totalCountIndexOnPage = 0;

              pageData.forEach(({ hours, cost }) => {
                totalCountIndexOnPage++;
                totalHoursOnPage += hours;
                totalCostOnPage += cost
              });
              return (
                <>
                  { totalCountIndexOnPage > 10 &&
                    <Table.Summary.Row>
                      <Table.Summary.Cell />
                      <Table.Summary.Cell align='center'>
                        <Text type="danger">{totalCountIndexOnPage}</Text>
                      </Table.Summary.Cell>
                      <Table.Summary.Cell colSpan={3} />
                      <Table.Summary.Cell align='center'>
                        <Text type="danger">{totalHoursOnPage}</Text>
                      </Table.Summary.Cell>
                      <Table.Summary.Cell align='center'>
                        <Text type="danger">${totalCostOnPage}</Text>
                      </Table.Summary.Cell>
                      <Table.Summary.Cell colSpan={2} />
                      <Table.Summary.Cell align='center'>
                        Total / Page
                    </Table.Summary.Cell>
                    </Table.Summary.Row>
                  }
                  <Table.Summary.Row>
                    <Table.Summary.Cell />
                    <Table.Summary.Cell align='center'>
                      <Text type="danger">{countIndex - 1}</Text>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={3} />
                    <Table.Summary.Cell align='center'>
                      <Text type="danger">{totalHours}</Text>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell align='center'>
                      <Text type="danger">${totalCost}</Text>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={2} />
                    <Table.Summary.Cell align='center'>
                      Total
                    </Table.Summary.Cell>
                  </Table.Summary.Row>
                </>
              );
            }}
        />

      </Fragment>
    )
  };

  return (
    <Root maxWidth="md">
      <Head title="My Hires" />

      {renderContent()}
    </Root>
  );
};

export default Hires;
