import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { generatePath } from 'react-router-dom';

import { useMutation } from '@apollo/client';
import { useStore } from 'store';

import { CREATE_HIRE } from 'graphql/hire';
import { GET_AUTH_USER } from 'graphql/user';


import { A } from 'components/Text';
import {
  Modal,
  Button,
  Divider,
  Form,
  Slider,
  InputNumber,
  Row,
  Col,
  Tooltip,
  Space,
  notification,
  Spin,
  Popover,
  Input,
} from 'antd';

import {
  PlusOutlined,
} from '@ant-design/icons';

import theme from 'theme';

import * as Routes from 'routes';

const Overlay = styled.div`
  position: absolute;
  width: 100%;
  height: 150px;
  top: 0;
  bottom: 0;
  opacity: 0;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  transition: opacity 0.3s, visibility 0.3s;
  background-color: rgba(0, 0, 0, 0.3);
  color: ${(p) => p.theme.colors.white};
`;

const Root = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  height: auto;
  background-color: white;
  border-radius: 7px;
  border: 1px solid ${(p) => p.theme.colors.border.main};
  transition: border-color 0.1s;
`;

const Photo = styled.div`
  width: 100%;
  height: 100%;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  background-color: ${(p) => p.theme.colors.grey[300]};
`;

const ImageContainer = styled.div`
  display: block;
  width: 100%;
  height: 150px;
  overflow: hidden;
  border-radius: 5px 5px 0 0;
  flex-shrink: 0;
  position: relative;
  overflow: hidden;
  &:hover ${Overlay} {
    opacity: 1;
  }
`;

const ContentCard = styled.div`
  padding: 10px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const TitleContentCard = styled.div`
  display: flex;
  flex-direction: row;
`;

const Author = styled(A)`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const Name = styled.span`
  font-size: ${(p) => p.theme.font.size.xs};
  font-weight: ${(p) => p.theme.font.weight.bold};
  color: ${(p) => p.theme.colors.primary.main};
`;

const InitialLetters = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  text-transform: uppercase;
  color: ${(p) => p.theme.colors.white};
  font-size: ${(p) => p.theme.font.size.lg};
  background-color: ${(p) => p.color};
`;

const Price = styled.span`
  padding-left: 10px;
  font-size: 18px;
  font-weight: 600;
`;

const LinkA = styled.a`
  color: #fff;
  padding: 10px 15px;
  border: 1px solid #fff;
  border-radius: 4px;
  cursor: pointer;

  :hover {
    border: 1px solid #f0564a;
    color: #fff;
  }
`

const GameListContainer = styled.div`
  display: flex;
  flex-direction: row;
`;


const GamesList = styled.span`
  font-size: 18x;
  padding-right: 5px;
`;
/**
 * Card component for rendering user info, meant to be used in Peoples page
 */
const StoreCard = ({ user }) => {
  const [{ auth }] = useStore();
  const [color, setColor] = useState('');
  const { fullName, username, image, price, games } = user;
  const [imageLoaded, setImageLoaded] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [loadingCreateHireForm, setLoadingCreateHireForm] = useState(false);
  const [{ hours }, setHours] = useState({ hours: 1 });
  const [{ message }, setMessage] = useState({ message: '' });
  const [componentSize, setComponentSize] = useState('default');

  const [createHire] = useMutation(CREATE_HIRE, {
    refetchQueries: [
      {
        query: GET_AUTH_USER
      }
    ]
  });

  const [form] = Form.useForm();
  const { TextArea } = Input;

  const gameToShow = games.slice(0, 3);

  useEffect(() => {
    const img = new Image();
    img.src = image;

    img.onload = () => {
      setImageLoaded(true);
    };

    return () => {
      img.onload = null;
    };
  }, [image]);

  useEffect(() => {
    const { primary, secondary, success, error } = theme.colors;
    const colors = [primary.main, secondary.main, success, error.main];
    const randomColor = Math.floor(Math.random() * colors.length);
    setColor(colors[randomColor]);
  }, []);

  const splitFullName = () => {
    // If a fullName contains more word than two, take first two word
    const splitWords = fullName.split(' ').slice(0, 2).join(' ');

    // Take only first letters from split words
    const firstLetters = splitWords
      .split(' ')
      .map((a) => a.charAt(0))
      .join(' ');

    return firstLetters;
  };

  const showModal = () => {
    setIsModalVisible(true);
    window.history.pushState('', '', generatePath(Routes.STORE, { username: user.username }));
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setHours({ hours: 1 });
    window.history.pushState('', '', generatePath(Routes.STORES));
  };

  const hoursRange = {
    1: '1 hours',
    24: '24'
  }

  const onChangeHours = (value) => {
    setHours({ hours: value });
  };

  const onChangeMessage = ({ target: { value } }) => {
    setMessage({ message: value });
  }

  const checkOut = (hours, price) => {
    return hours * price;
  };

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const handleReset = () => {
    setHours({ hours: 1 });
    setMessage({ message: '' });
    setLoadingCreateHireForm(false);
    openCreateHireNotification('Creating a hire', 'successfully');
    setIsModalVisible(false);
    // window.history.pushState('', '', generatePath(Routes.HIRES));
    window.location.href = `${Routes.HIRES}`
  };

  const openCreateHireNotification = (msg, status) => {
    notification.info({
      message: `${msg} ${status}`,
      description: `You rented ${hours} from ${user.fullName} for $ ${user.price}`,
    });
  };

  const handleSubmitHire = async () => {
    try {
      await createHire({
        variables: {
          input: { clientId: auth.user.id, hiredPersonId: user.id, hours: hours, message: message }
        },
      });
      handleReset();
    } catch (error) {

    }
  };

  const layout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 16,
    },
  };

  const tailLayout = {
    wrapperCol: {
      offset: 6,
      span: 16,
    },
  };

  return (
    <Root>
      <ImageContainer>
        {image
          ? <Photo style={imageLoaded ? { backgroundImage: `url(${image})` } : {}} />
          : <InitialLetters color={color}>{splitFullName()}</InitialLetters>}
        <Overlay>
          <LinkA onClick={showModal}>
            <span>Hire Me</span>
          </LinkA>
        </Overlay>
      </ImageContainer>

      <ContentCard>
        <TitleContentCard>
          <Author
            to={generatePath(Routes.USER_PROFILE, { username })}
          >
            <Name>{fullName}</Name>
          </Author>
          <Price>${price}</Price>
        </TitleContentCard>
        <GameListContainer>
          {gameToShow.map((game) => (
            <GamesList key={game.name}>@{game.name}</GamesList>
          ))}
        </GameListContainer>
        <Popover
          content={<div>{games.map((game) => `@${game.name} `)}</div>}
          title="Games"
          placement="bottom">
          More games
        </Popover>

      </ContentCard>
      {/* {isModalVisible && <HireMeModal user={user} modalVisible={isModalVisible} />} */}
      <Modal
        title={`Hire ${user.fullName}`}
        visible={isModalVisible}
        centered
        width={400}
        onCancel={handleCancel}
        footer={null}>
        <Spin spinning={loadingCreateHireForm} size="large">
          <Form
            {...layout}
            form={form}
            onFinish={handleSubmitHire}
            initialValues={{
              size: componentSize,
            }}
            onValuesChange={onFormLayoutChange}
            size={componentSize}
          >
            <Form.Item label='Balance'>
              <Space>

                {`$ ${auth.user.balance}`}
                <Tooltip title="Top-up">
                  <Button
                    type="primary"
                    shape="circle"
                    size="small"
                    icon={<PlusOutlined />}
                  />
                </Tooltip>
              </Space>
            </Form.Item>
            <Form.Item label='Price'>
              {`$ ${user.price}`}
            </Form.Item>
            <Form.Item label='Hours'>

              <InputNumber
                min={1}
                max={24}
                value={hours}
                onChange={onChangeHours}
              />
              <Row>
                <Col span={18}>
                  <Slider
                    min={1}
                    max={24}
                    marks={hoursRange}
                    defaultValue={1}
                    onChange={onChangeHours}
                    value={typeof hours === 'number' ? hours : 0}
                  />
                </Col>
              </Row>
            </Form.Item>

            <Form.Item label='Message'>
              <TextArea
                value={message}
                onChange={onChangeMessage}
                placeholder="Give message for player"
                autoSize={{ minRows: 3, maxRows: 5 }}
              />
            </Form.Item>

            <Divider />
            <Form.Item
              label='Checkout'
              validateStatus={auth.user.balance < checkOut(hours, user.price) ? 'error' : ''}
              help={auth.user.balance < checkOut(hours, user.price) ? 'Your balance is not enough to pay' : ''}
            >
              <strong>{`$ ${checkOut(hours, user.price)}`}</strong>
            </Form.Item>

            <Form.Item {...tailLayout}>
              <Button
                type="primary"
                htmlType="submit"
                onClick={() => setLoadingCreateHireForm(true)}
                disabled={auth.user.balance < checkOut(hours, user.price) ? true : false} >
                Submit
            </Button>
            </Form.Item>

          </Form>
        </Spin>
      </Modal>
    </Root >
  );
};

StoreCard.propTypes = {
  user: PropTypes.object.isRequired,
};

export default StoreCard;
