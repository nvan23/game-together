import React, { useState, useEffect, Fragment } from 'react';
import { Link, generatePath } from 'react-router-dom';
import styled from 'styled-components';
import { useApolloClient } from '@apollo/client';

import { Container } from 'components/Layout';
import Skeleton from 'components/Skeleton';
import Empty from 'components/Empty';
import Head from 'components/Head';

import { timeAgo } from 'utils/date';
import {
  getHiredIdToFocusOnTable,
  addHiredIdToFocusOnTable,
  setTargetTime,
} from 'utils/cache';

import {
  Table,
  Typography,
  Avatar,
  Image,
  Steps,
  Divider,
  Button,
  Tag,
  Descriptions,
  Rate,
} from 'antd';

import {
  DownOutlined,
  UpOutlined,
  CheckCircleOutlined,
  SyncOutlined,
  CloseCircleOutlined,
  ClockCircleOutlined,
  CheckOutlined,
} from "@ant-design/icons";

import CountDown from 'ant-design-pro/lib/CountDown';

import {
  GET_ALL_HIRES_TO_WORK,
  GET_ALL_HIRES_TO_WORK_SUBSCRIPTION,
  REFUND_REQUEST_TO_LESSOR_SUBSCRIPTION,
  CHANGE_READY_CLIENT_STATUS,
  CHANGE_STAGE_SUBSCRIPTION,
  OVER_TIME_OUT_SUBSCRIPTION,
  GET_HIRE,
  FEEDBACK_CREATED,
  REPLY_REFUND_REQUEST,
} from 'graphql/hire';

import { PEOPLE_PAGE_USERS_LIMIT } from 'constants/DataLimit';
import { WAIT, ONGOING, CLOSE } from 'constants/Hire';

import { useQuery } from '@apollo/client';

import { useStore } from 'store';

import * as Routes from 'routes';

import { A } from 'components/Text';

const Root = styled(Container)`
  margin-top: ${(p) => p.theme.spacing.lg};

  @media (min-width: ${(p) => p.theme.screen.lg}) {
    margin-left: ${(p) => p.theme.spacing.lg};
    padding: 0;
  }
`;

const PeopleContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(180px, 3fr));
  grid-auto-rows: auto;
  grid-gap: 20px;
  margin-bottom: ${(p) => p.theme.spacing.lg};
`;

const Message = styled(Link)`
  text-decoration: none;
  font-size: ${(p) => p.theme.font.size.xs};
`;

const Name = styled.span`
  font-size: ${(p) => p.theme.font.size.xs};
  font-weight: ${(p) => p.theme.font.weight.bold};
  color: ${(p) => p.theme.colors.primary.main};
`;

const ExpandableContainer = styled.div`
  padding: 20px 40px;
`
const StepContainer = styled.div`
  padding: 0 0 10px 30px;
`
const ButtonStepContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin: 20px 0 0 30px;
`

const ButtonContainer = styled.div`
  margin-left: 20px;
`

const FeedbackContainer = styled.div`
  display: block;
`

/**
 * Workplace where player receive hires 
 */
const Workplace = () => {
  const [{ auth }] = useStore();
  const client = useApolloClient();

  const { subscribeToMore, data: HiresToWork, loading, networkStatus } = useQuery(GET_ALL_HIRES_TO_WORK, {
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
  });

  useEffect(() => {
    const unsubscribe = subscribeToMore({
      document: GET_ALL_HIRES_TO_WORK_SUBSCRIPTION,
      variables: { authUserId: auth.user.id },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const newHireToWork = subscriptionData.data.hireCreated;
        const mergedHiresToWork = [...prev.getAllHiresToWork, newHireToWork];

        return { getAllHiresToWork: mergedHiresToWork };
      },
    });

    return () => {
      unsubscribe();
    };
  }, [auth.user.id, subscribeToMore]);

  useEffect(() => {
    const unsubscribe = subscribeToMore({
      document: CHANGE_STAGE_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const hireChanged = subscriptionData.data.stageChangedSubscription;
        const mergedHires = [...prev.getAllHiresToWork, hireChanged];

        return { getAllHiresToWork: mergedHires };
      },
    });

    return () => {
      unsubscribe();
    };
  }, [subscribeToMore]);

  useEffect(() => {
    const unsubscribe = subscribeToMore({
      document: OVER_TIME_OUT_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const hireChanged = subscriptionData.data.overTimeoutSubscription;
        const mergedHires = [...prev.getAllHiresToWork, hireChanged];

        return { getAllHiresToWork: mergedHires };
      },
    });

    return () => {
      unsubscribe();
    };
  }, [subscribeToMore]);

  useEffect(() => {
    const unsubscribe = subscribeToMore({
      document: FEEDBACK_CREATED,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const hireChanged = subscriptionData.data.feedbackCreated;
        const mergedHires = [...prev.getAllHiresToWork, hireChanged];

        return { getAllHiresToWork: mergedHires };
      },
    });

    return () => {
      unsubscribe();
    };
  }, [subscribeToMore]);

  useEffect(() => {
    const unsubscribe = subscribeToMore({
      document: REFUND_REQUEST_TO_LESSOR_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const hireChanged = subscriptionData.data.sendRefundRequestToLessorSubscription;
        const mergedHires = [...prev.getAllHiresToWork, hireChanged];

        return { getAllHiresToWork: mergedHires };
      },
    });

    return () => {
      unsubscribe();
    };
  }, [subscribeToMore]);

  const { Text, Title } = Typography;
  const { Step } = Steps;

  const renderContent = () => {
    if (loading && networkStatus === 1) {
      return (
        <PeopleContainer>
          <Skeleton height={280} count={PEOPLE_PAGE_USERS_LIMIT} />
        </PeopleContainer>
      );
    }

    const hires = HiresToWork.getAllHiresToWork;

    let countIndex = 1;
    if (!hires.length > 0) return <Empty text="No hire yet." />;

    const dataSourceHireToWorkTable = hires.map((hire) => {
      const unitHire = {
        index: countIndex++,
        key: hire.id,
        name: hire.id,
        playerId: hire.hiredPersonId.id,
        clientId: hire.clientId.id,
        avatarClient: hire.clientId.image,
        usernameClient: hire.clientId.username,
        feedback: hire.feedbackId ? hire.feedbackId.content : '',
        starRate: hire.feedbackId ? hire.feedbackId.star : 0,
        feedbackStatus: hire.feedbackStatus,
        price: hire.price,
        hours: hire.hours,
        timeout: hire.timeout,
        cost: hire.cost,
        message: hire.message,
        messageToThank: hire.messageToThank,
        readyClientStatus: hire.readyClientStatus,
        stage: hire.stage,
        status: hire.status,
        beReported: hire.beReported,
        reportId: hire.reportId === null ? '' : hire.reportId.id,
        refundRequestFromClient: hire.reportId === null ? false : hire.reportId.refundRequest,
        refundRequestStatus: hire.refundRequestStatus,
        createdAt: hire.createdAt,
      }
      return unitHire;
    })

    let uniqueUsernameClient = [...new Set(hires.map((hire) => hire.clientId.username))];
    const usernameClientToFilter = uniqueUsernameClient.map((usernameClient) => {
      const user = {
        text: usernameClient,
        value: usernameClient,
      }
      return user;
    });

    const stageToFilter = [
      {
        text: 'Wait',
        value: 'wait',
      },
      {
        text: 'Ongoing',
        value: 'ongoing',
      },
      {
        text: 'Close',
        value: 'close',
      },
    ];

    const statusToFilter = [
      {
        text: 'Ongoing',
        value: 'ongoing',
      },
      {
        text: <Text type="success">Success</Text>,
        value: 'success',
      },
      {
        text: <Text type="danger">Fail</Text>,
        value: 'fail',
      },
    ];

    const beReportedToFilter = [
      {
        text: 'Be reported',
        value: true,
      },
      {
        text: 'None',
        value: false,
      },
    ];

    const columns = [
      {
        title: 'Index',
        dataIndex: 'index',
        key: 'index',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.index - b.index,
      },
      {
        title: 'Avatar',
        dataIndex: 'avatarClient',
        key: 'avatarClient',
        align: 'center',
        render: avatarClient => (
          <Avatar
            src={<Image src={avatarClient} />}
            size={40}
          />
        )
      },
      {
        title: 'Client',
        dataIndex: 'usernameClient',
        key: 'usernameClient',
        align: 'center',
        filters: usernameClientToFilter,
        onFilter: (value, record) => record.usernameClient.indexOf(value) === 0,
        render: usernameClient => (
          <A to={generatePath(Routes.USER_PROFILE, { username: usernameClient })}>
            <Name>{usernameClient}</Name>
          </A>
        )
      },
      {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.price - b.price,
        render: price => (`$${price}`)
      },
      {
        title: 'Hours',
        dataIndex: 'hours',
        key: 'hours',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.hours - b.hours,
      },
      {
        title: 'Cost',
        dataIndex: 'cost',
        key: 'cost',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.cost - b.cost,
        render: cost => (`$${cost}`)
      },
      {
        title: 'Stage',
        dataIndex: 'stage',
        key: 'stage',
        align: 'center',
        filters: stageToFilter,
        onFilter: (value, record) => record.stage.indexOf(value) === 0,
        render: stage => (
          stage === 'close'
            ? <Text>Close</Text>
            : (stage === 'wait'
              ? (<Tag icon={<ClockCircleOutlined />} color="default">Wait</Tag>)
              : (<Tag icon={<SyncOutlined />} color="processing">Ongoing</Tag>))
        )
      },
      {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
        align: 'center',
        filters: statusToFilter,
        onFilter: (value, record) => record.status.indexOf(value) === 0,
        render: status => (
          status === 'success'
            ? (<Tag icon={<CheckCircleOutlined />} color="success">Success</Tag>)
            : (status === 'fail'
              ? (<Tag icon={<CloseCircleOutlined />} color="error">Fail</Tag>)
              : (<Tag icon={<SyncOutlined />} color="processing">Ongoing</Tag>))
        )
      },
      {
        title: 'Be reported',
        dataIndex: 'beReported',
        key: 'beReported',
        align: 'center',
        filters: beReportedToFilter,
        onFilter: (value, record) => record.beReported.indexOf(value) === 0,
        render: beReported => (
          beReported
            ? <CheckOutlined />
            : ''
        ),
      },
      {
        title: 'Created At',
        dataIndex: 'createdAt',
        key: 'createdAt',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => parseInt(a.createdAt) - parseInt(b.createdAt),
        render: createdAt => (
          <>
            {/* for Vietnam, set value in toLocalDateString is "vi-VN": "dd/MM/yyyy" */}
            {`${timeAgo(createdAt)} ago (${new Date(parseInt(createdAt)).toLocaleDateString("en-US")})`}
          </>
        )
      },
    ];

    const handleCurrentStep = (stage) => {
      switch (stage) {
        case WAIT:
          return 1
        case ONGOING:
          return 2
        case CLOSE:
          return 4
        default:
          return 0
      }
    }

    const targetTime = new Date().getTime() + 3900000;

    let totalHours = 0;
    let totalCost = 0;

    dataSourceHireToWorkTable.forEach(({ hours, cost }) => {
      totalHours += hours;
      totalCost += cost;
    });

    return (
      <Fragment>
        <Table
          bordered
          size="middle"
          rowKey="key"
          loading={dataSourceHireToWorkTable.length > 0 ? false : true}
          columns={columns}
          expandable={{

            expandedRowRender: record => (
              <ExpandableContainer>
                <StepContainer>
                  <Steps current={handleCurrentStep(record.stage)} percent={0} size='small'>
                    <Step
                      title="Ordered"
                      description={
                        <>
                          Find and order hire services on
                          <Message to={generatePath(Routes.STORES)}> Stores.</Message>
                        </>
                      }
                    />
                    <Step title="Wait" description="Wait for players to get ready" />
                    <Step title="Ongoing" description={
                      <>
                        {
                          record.stage === ONGOING
                            ? <CountDown style={{ fontSize: 20 }} target={setTargetTime(record.hours, record.timeout)} />
                            : ''
                        }
                      </>
                    }
                    />
                    <Step title="Close" description="End the hiring process at TIME" />
                  </Steps>
                  <ButtonStepContainer>
                    <Button
                      type="primary"
                      disabled={record.readyClientStatus}
                      onClick={
                        async () => {
                          try {
                            await client.mutate({
                              mutation: CHANGE_READY_CLIENT_STATUS,
                              variables: { input: { hireId: record.key, } },
                              refetchQueries: () => [
                                { query: GET_ALL_HIRES_TO_WORK },
                                // { query: GET_HIRE, variables: { hireId: record.key } },
                              ],
                            });
                            addHiredIdToFocusOnTable(record.key);
                          } catch (err) { }
                        }
                      }
                    >
                      Ready
                    </Button>
                    <ButtonContainer>
                      <Button
                        danger>
                        Report
                      </Button>
                    </ButtonContainer>
                    <ButtonContainer>
                      <Button
                        disabled={!record.refundRequestFromClient}
                        onClick={
                          async () => {
                            try {
                              await client.mutate({
                                mutation: REPLY_REFUND_REQUEST,
                                variables: {
                                  input: {
                                    clientId: record.clientId,
                                    hiredPersonId: record.playerId,
                                    hireId: record.key,
                                    reportId: record.reportId,
                                    value: true,
                                    hours: record.hours,
                                  }
                                },
                              });
                            } catch (err) { }
                          }
                        }
                      >
                        Refund
                      </Button>
                      <Button
                        type='link'
                        danger
                        disabled={!record.refundRequestFromClient}>
                        or DISAGREE
                      </Button>
                    </ButtonContainer>
                  </ButtonStepContainer>
                </StepContainer>
                <Divider />
                <Title level={5}>{`HIRE ID: ${record.key.toUpperCase()}`}</Title>
                <p>
                  <Text strong >{` ${record.usernameClient}: `}</Text>{record.message}
                </p>
                <p>
                  <Text strong >{` ${auth.user.username}: `}</Text>{record.messageToThank}
                </p>
                <Message to={generatePath(Routes.MESSAGES, { userId: record.clientId })}>Chat more...</Message>
                <Divider />
                {record.feedbackStatus
                  ? <FeedbackContainer>
                    <Descriptions title='Feedback' bordered>
                      <Descriptions.Item label="From" span={3}>
                        <A to={generatePath(Routes.USER_PROFILE, { username: record.usernameClient })}>
                          <Name>{record.usernameClient}</Name>
                        </A>
                      </Descriptions.Item>
                      <Descriptions.Item label="Status" span={3}>
                        <Rate
                          disabled={true}
                          value={record.starRate}
                        />
                      </Descriptions.Item>
                      <Descriptions.Item label="Content">
                        {record.feedback}
                      </Descriptions.Item>
                    </Descriptions>
                  </FeedbackContainer>
                  : 'There is no have feedback'}
              </ExpandableContainer>
            ),
            expandableRow: () => { },
            defaultExpandedRowKeys: getHiredIdToFocusOnTable(),
            expandIcon: ({ expanded, onExpand, record }) =>
              expanded
                ? (<UpOutlined onClick={e => onExpand(record, e)} />)
                : (<DownOutlined onClick={e => onExpand(record, e)} />),
          }}
          dataSource={dataSourceHireToWorkTable}
          summary={
            pageData => {
              let totalHoursOnPage = 0;
              let totalCostOnPage = 0;
              let totalCountIndexOnPage = 0;

              pageData.forEach(({ hours, cost }) => {
                totalCountIndexOnPage++;
                totalHoursOnPage += hours;
                totalCostOnPage += cost
              });
              return (
                <>
                  { totalCountIndexOnPage > 10 &&
                    <Table.Summary.Row>
                      <Table.Summary.Cell />
                      <Table.Summary.Cell align='center'>
                        <Text type="danger">{totalCountIndexOnPage}</Text>
                      </Table.Summary.Cell>
                      <Table.Summary.Cell colSpan={3} />
                      <Table.Summary.Cell align='center'>
                        <Text type="danger">{totalHoursOnPage}</Text>
                      </Table.Summary.Cell>
                      <Table.Summary.Cell align='center'>
                        <Text type="danger">${totalCostOnPage}</Text>
                      </Table.Summary.Cell>
                      <Table.Summary.Cell colSpan={2} />
                      <Table.Summary.Cell align='center'>
                        Total / Page
                    </Table.Summary.Cell>
                    </Table.Summary.Row>
                  }
                  <Table.Summary.Row>
                    <Table.Summary.Cell />
                    <Table.Summary.Cell align='center'>
                      <Text type="danger">{countIndex - 1}</Text>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={3} />
                    <Table.Summary.Cell align='center'>
                      <Text type="danger">{totalHours}</Text>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell align='center'>
                      <Text type="danger">${totalCost}</Text>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={2} />
                    <Table.Summary.Cell align='center'>
                      Total
                    </Table.Summary.Cell>
                  </Table.Summary.Row>
                </>
              );
            }}
        />
      </Fragment>
    )
  };

  return (
    <Root maxWidth="md">
      <Head title="Workplace" />

      {renderContent()}
    </Root>
  );
};

export default Workplace;
