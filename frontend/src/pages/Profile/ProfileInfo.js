import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link, generatePath } from 'react-router-dom';
import { useMutation, useSubscription } from '@apollo/client';

import { GET_AUTH_USER, IS_USER_ONLINE_SUBSCRIPTION } from 'graphql/user';

import {
  Modal,
  Button,
  Divider,
  Form,
  Slider,
  InputNumber,
  Row,
  Col,
  Tooltip,
  Space,
  notification,
  Spin,
  Input,
} from 'antd';

import {
  PlusOutlined,
} from '@ant-design/icons';

import { CREATE_HIRE } from 'graphql/hire';

import { H1 } from 'components/Text';
import { Spacing } from 'components/Layout';
import Follow from 'components/Follow';
import ProfileImageUpload from './ProfileImageUpload';
import ProfileCoverUpload from './ProfileCoverUpload';

import { useStore } from 'store';

import * as Routes from 'routes';

const Root = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const ProfileImage = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: -140px;
`;

const FullName = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-top: ${(p) => p.theme.spacing.sm};
  position: relative;

  ${H1} {
    font-size: ${(p) => p.theme.font.size.lg};
  }

  @media (min-width: ${(p) => p.theme.screen.md}) {
    ${H1} {
      font-size: ${(p) => p.theme.font.size.xl};
    }
  }
`;

const FollowAndMessage = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-left: ${(p) => p.theme.spacing.sm};
`;

const Message = styled(Link)`
  text-decoration: none;
  font-size: ${(p) => p.theme.font.size.xs};
`;

const Online = styled.div`
  width: 8px;
  height: 8px;
  background-color: ${(p) => p.theme.colors.success};
  margin-left: ${(p) => p.theme.spacing.sm};
  border-radius: 50%;
`;

const Info = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  font-size: ${(p) => p.theme.font.size.xs};
  margin-top: ${(p) => p.theme.spacing.sm};
`;

const List = styled.div`
  padding: 0 ${(p) => p.theme.spacing.xs};
  color: ${(p) => p.theme.colors.grey[800]};
  white-space: nowrap;

  @media (min-width: ${(p) => p.theme.screen.md}) {
    padding: 0 ${(p) => p.theme.spacing.lg};
  }
`;

/**
 * Renders user information in profile page
 */
const ProfileInfo = ({ user }) => {
  const [{ auth }] = useStore();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [loadingCreateHireForm, setLoadingCreateHireForm] = useState(false);
  const [{ hours }, setHours] = useState({ hours: 1 });
  const [{ message }, setMessage] = useState({ message: '' });
  const [componentSize, setComponentSize] = useState('default');

  const [createHire] = useMutation(CREATE_HIRE, {
    refetchQueries: [
      {
        query: GET_AUTH_USER
      }
    ]
  });

  const [form] = Form.useForm();
  const { TextArea } = Input;

  const { data, loading } = useSubscription(IS_USER_ONLINE_SUBSCRIPTION, {
    variables: { authUserId: auth.user.id, userId: user.id },
  });

  let isUserOnline = user.isOnline;
  if (!loading && data) {
    isUserOnline = data.isUserOnline.isOnline;
  }

  const showModal = () => {
    setIsModalVisible(true);
    window.history.pushState('', '', generatePath(Routes.STORE, { username: user.username }));
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    window.history.pushState('', '', generatePath(Routes.USER_PROFILE, { username: user.username }));
    setHours({ hours: 1 });
  };

  const hoursRange = {
    1: '1 hours',
    24: '24'
  }

  const onChangeHours = (value) => {
    setHours({ hours: value });
  };

  const onChangeMessage = ({ target: { value } }) => {
    setMessage({ message: value });
  }


  const checkOut = (hours, price) => {
    return hours * price;
  };

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const handleReset = () => {
    setHours({ hours: 1 });
    setMessage({ message: '' });
    setLoadingCreateHireForm(false);
    openCreateHireNotification('Creating a hire', 'successfully');
    setIsModalVisible(false);
  };

  const openCreateHireNotification = (msg, status) => {
    notification.info({
      message: `${msg} ${status}`,
      description: `You rented ${hours} from ${user.fullName} for $ ${user.price}`,
    });
  };

  const handleSubmitHire = async () => {
    try {
      await createHire({
        variables: {
          input: { clientId: auth.user.id, hiredPersonId: user.id, hours: hours, message: message }
        },
      });
      handleReset();
    } catch (error) {

    }
  };

  const layout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 16,
    },
  };

  const tailLayout = {
    wrapperCol: {
      offset: 6,
      span: 16,
    },
  };

  return (
    <Root>
      <ProfileCoverUpload userId={user.id} coverImage={user.coverImage} coverImagePublicId={user.coverImagePublicId} />

      <ProfileImage>
        <ProfileImageUpload
          userId={user.id}
          image={user.image}
          imagePublicId={user.imagePublicId}
          username={user.username}
        />

        <FullName>
          <H1>{user.fullName}</H1>
          {isUserOnline && auth.user.id !== user.id && <Online />}
        </FullName>

        <Spacing top='sm' />

        <div>
          {auth.user.id !== user.id && (
            <FollowAndMessage>
              <Follow user={user} />
              <Spacing left='sm' />
              <Button type='primary' danger onClick={showModal} size='small'>
                Hire me
              </Button>
              <Spacing left='sm' />
              <Message to={generatePath(Routes.MESSAGES, { userId: user.id })}>Message</Message>
              <Spacing left='sm' />
            </FollowAndMessage>
          )}
        </div>
      </ProfileImage>

      <Info>
        <List>
          <b>{user.posts.length} </b> posts
        </List>
        <List>
          <b>{user.followers.length} </b> followers
        </List>
        <List>
          <b>{user.following.length} </b> following
        </List>
      </Info>
      <Modal
        title={`Hire ${user.fullName}`}
        visible={isModalVisible}
        centered
        width={400}
        onCancel={handleCancel}
        footer={null}>
        <Spin spinning={loadingCreateHireForm} size="large">
          <Form
            {...layout}
            form={form}
            onFinish={handleSubmitHire}
            initialValues={{
              size: componentSize,
            }}
            onValuesChange={onFormLayoutChange}
            size={componentSize}
          >
            <Form.Item label='Balance'>
              <Space>

                {`$ ${auth.user.balance}`}
                <Tooltip title="Top-up">
                  <Button
                    type="primary"
                    shape="circle"
                    size="small"
                    icon={<PlusOutlined />}
                  />
                </Tooltip>
              </Space>
            </Form.Item>
            <Form.Item label='Price'>
              {`$ ${user.price}`}
            </Form.Item>
            <Form.Item label='Hours'>

              <InputNumber
                min={1}
                max={24}
                value={hours}
                onChange={onChangeHours}
              />
              <Row>
                <Col span={18}>
                  <Slider
                    min={1}
                    max={24}
                    marks={hoursRange}
                    defaultValue={1}
                    onChange={onChangeHours}
                    value={typeof hours === 'number' ? hours : 0}
                  />
                </Col>
              </Row>
            </Form.Item>

            <Form.Item label='Message'>
              <TextArea
                value={message}
                onChange={onChangeMessage}
                placeholder="Give message for player"
                autoSize={{ minRows: 3, maxRows: 5 }}
              />
            </Form.Item>

            <Divider />
            <Form.Item
              label='Checkout'
              validateStatus={auth.user.balance < checkOut(hours, user.price) ? 'error' : ''}
              help={auth.user.balance < checkOut(hours, user.price) ? 'Your balance is not enough to pay' : ''}
            >
              <strong>{`$ ${checkOut(hours, user.price)}`}</strong>
            </Form.Item>

            <Form.Item {...tailLayout}>
              <Button
                type="primary"
                htmlType="submit"
                onClick={() => setLoadingCreateHireForm(true)}
                disabled={auth.user.balance < checkOut(hours, user.price) ? true : false} >
                Submit
            </Button>
            </Form.Item>

          </Form>
        </Spin>
      </Modal>
    </Root >
  );
};

ProfileInfo.propTypes = {
  user: PropTypes.object.isRequired,
};

export default ProfileInfo;
