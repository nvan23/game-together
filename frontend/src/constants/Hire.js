export const PRICE_DEFAULT = 2;

export const WAIT = 'wait';
export const ONGOING = 'ongoing';
export const CLOSE = 'close';

export const SUCCESS = 'success';
export const FAILURE = 'failure';
