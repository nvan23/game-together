/**
 * Actions types
 */
export const SET_HIRE_ID = 'SET_HIRE_ID';
export const CLEAR_HIRE_ID = 'CLEAR_HIRE_ID';

/**
 * Initial State
 */
export const authInitialState = {
  hireId: '',
};

/**
 * User Reducer
 */
export const authReducer = (state = authInitialState, action) => {
  switch (action.type) {
    case SET_HIRE_ID:
      return {
        ...state,
        hireId: action.payload,
      };

    case CLEAR_HIRE_ID:
      return {
        ...state,
        ...authInitialState,
      };

    default:
      return state;
  }
};
