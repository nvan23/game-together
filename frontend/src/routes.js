/**
 * All available routes for the app
 */

export const HOME = '/';

export const FORGOT_PASSWORD = '/forgot-password';

export const RESET_PASSWORD = '/reset-password';

export const USER_PROFILE = '/:username';

export const EXPLORE = '/explore';

export const PEOPLE = '/people';

export const STORES = '/stores';

export const HIRES = '/hires';

export const WORKPLACE = '/workplace';

export const STORE = 'stores/:username';

export const NOTIFICATIONS = '/notifications';

export const MESSAGES = '/messages/:userId';

export const POST = '/post/:id';

/**
 * Value that's used in place of id when creating something new.
 */
export const NEW_ID_VALUE = 'new';
