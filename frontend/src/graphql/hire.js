import { gql } from '@apollo/client';

const User = `
  id
  username
  image
`

const Feedback = `
  id
  content
  star
`

const Report = `
  id
  refundRequest
  responded
  relyRefundRequest
`

const hiresPayload = `
  id
  clientId {
    ${User}
  }
  hiredPersonId {
    ${User}
  }
  timeout
  feedbackId {
    ${Feedback}
  }
  reportId {
    ${Report}
  }
  feedbackStatus
  price
  hours
  cost
  message
  messageToThank
  readyClientStatus
  beReported
  refundRequestStatus
  stage
  status
  createdAt
`

const timeoutPayload = `
  key
  value
  createdAt
  updatedAt
`

/**
 * Get my all hires
 */
export const GET_ALL_HIRES = gql`
  query {
    getAllHires {
      count
      hires {
        ${hiresPayload}
      }
    }
  }
`;

/**
 * Get all hires of client
 */
export const GET_ALL_HIRES_TO_WORK = gql`
  query {
    getAllHiresToWork {
      ${hiresPayload}
    }
  }
`;

export const GET_ALL_TIME_OUT_CONTAINER = gql`
  query {
    getAllTimeoutContainers {
      count
      timeoutContainers {
        ${timeoutPayload}
      }
    }
  }
`;

export const GET_TIME_OUT_CONTAINER = gql`
  query ($key: String!) {
    getTimeoutContainer (key: $key) {
      id
      key
      value
      createdAt
      updatedAt
    }
  }
`;

/**
 * Get a hire
 */
export const GET_HIRE = gql`
  query($hireId: String!) {
    getHire(hireId: $hireId) {
      timeout
      hire {
        ${hiresPayload}
      }
    }
  }
`;

/**
 * Creates a hire relationship between two users
 */
export const CREATE_HIRE = gql`
  mutation($input: CreateHireInput!) {
    createHire(input: $input) {
      id
    }
  }
`;

export const CHANGE_READY_CLIENT_STATUS = gql`
  mutation($input: ChangeReadyClientStatusInput) {
    changeReadyClientStatus(input: $input) {
      ${hiresPayload}
    }
  }
`;

export const CHANGE_HIRE_STAGE = gql`
  mutation($input: ChangeHireStateInput) {
    changeHireState(input: $input) {
      ${hiresPayload}
    }
  }
`;

export const REPLY_REFUND_REQUEST = gql`
  mutation($input: ReplyRefundRequestInput) {
    replyRefundRequest(input: $input) {
      ${hiresPayload}
    }
  }
`;


/**
 * Close a hire relationship between two users
 */
export const CLOSE_HIRE = gql`
  mutation($input: CloseHireInput!) {
    closeHire(input: $input) {
      id
    }
  }
`;

export const CLEAR_TIMEOUT_HIRE = gql`
  mutation($input: CloseHireInput!) {
    clearTimeoutHire(input: $input) {
      perform
    }
  }
`;

/**
 * Report a hire relationship between two users
 */
export const REPORT_HIRE = gql`
  mutation($input: ReportHireInput!) {
    reportHire(input: $input) {
      id
    }
  }
`;

/**
 * Get my all hires in real time
 */
export const GET_ALL_HIRES_TO_WORK_SUBSCRIPTION = gql`
  subscription($authUserId: ID!) {
    hireCreated(authUserId: $authUserId) {
      ${hiresPayload}
    }
  }
`;

export const CHANGE_READY_CLIENT_STATUS_SUBSCRIPTION = gql`
  subscription {
    changeReadyClientStatusSubscription {
      ${hiresPayload}
    }
  }
`;

export const CHANGE_STAGE_SUBSCRIPTION = gql`
  subscription {
    stageChangedSubscription {
      ${hiresPayload}
    }
  }
`;

export const OVER_TIME_OUT_SUBSCRIPTION = gql`
  subscription {
    overTimeoutSubscription {
      ${hiresPayload}
    }
  }
`;

export const FEEDBACK_CREATED = gql`
  subscription {
    feedbackCreated {
      ${hiresPayload}
    }
  }
`;

export const CREATE_REPORT = gql`
  mutation($input: CreateReportClientInput!) {
    createReportClient(input: $input) {
      id
    }
  }
`;

export const REFUND_REQUEST_TO_LESSOR_SUBSCRIPTION = gql`
  subscription {
    sendRefundRequestToLessorSubscription {
      ${hiresPayload}
    }
  }
`;
