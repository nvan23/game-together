import { gql } from '@apollo/client';

const Feedback = `
  id
  content
  hours
  star
  createdAt
`

export const CREATE_FEEDBACK = gql`
  mutation($input: CreateFeedbackInput) {
    createFeedback(input: $input) {
      ${Feedback}
    }
  }
`;